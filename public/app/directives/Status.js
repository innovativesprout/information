(function(){
    "use strict";

    angular.module('Status', ['ngAnimate', 'ui.bootstrap', 'AngularVersioning'])

        .config(function($interpolateProvider, $httpProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
            $httpProvider.interceptors.push('preventTemplateCache');
        })

        .constant('BASE', {
            'API_URL': '/',
            'ASSETS_URL': 'app/register/templates/',
        })
        .directive('status', Status)

    function Status(BASE) {

        return {
            restrict: 'EA',
            scope: {
              statusCode: '@'
            },
            link: function (scope, element, attributes) {
                let success = ['Booked', 'Complete', 'Picked Up', 'Manifested'];
                let danger = ['Cancelled', 'Damaged', 'Lost', 'Tracking Expired'];
                let light = ['At Delivery', 'At Pickup', 'On For Delivery', 'Partial Delivery', 'Partial On For Delivery', 'In Transit', 'Sorted for Delivery', 'Unmanifested'];
                let warning = ['Delayed', 'Delivery Attempted', 'Delivery Time Scheduled', 'Scanned into Depot'];

                if(success.indexOf(scope.statusCode) !== -1){
                    element.addClass('badge badge-success')
                }else if(danger.indexOf(scope.statusCode) !== -1){
                    element.addClass('badge badge-danger')
                }else if(light.indexOf(scope.statusCode) !== -1){
                    element.addClass('badge badge-light')
                }else if(warning.indexOf(scope.statusCode) !== -1){
                    element.addClass('badge badge-warning')
                }
            }
        }

    }
})();
