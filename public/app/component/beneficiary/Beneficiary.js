(function(){
    "use strict";

    angular.module('Beneficiary', ['ui.bootstrap', 'AngularVersioning', 'SwalError',
        'RegionRepository','ProvinceRepository','CityRepository','BarangayRepository', 'IdentificationTypeRepository',
        'BeneficiaryTypeRepository', 'OccupationRepository', 'BeneficiaryRepository', 'CivilStatusRepository',
        'ui.mask'])

    .config(function($interpolateProvider, $httpProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
        $httpProvider.interceptors.push('preventTemplateCache');
    })

    .constant('BASE', {
        'API_URL': '/',
        'ASSETS_URL': 'app/component/beneficiary/templates/',
    })

    .directive('beneficiary', Beneficiary)

    function Beneficiary(BASE) {

        return {
            restrict: 'EA',
            controller: BeneficiaryController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'index.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function BeneficiaryController($scope, $window, $timeout, $filter, $uibModal, BASE, BeneficiaryRepository) {

        $scope.isLoading = false;
        $scope.buttonClicked = false;
        $scope.orders = [];

        $scope.pagination = {
            totalItems: 0,
            currentPage: 1,
            itemsPerPage: 10,
            maxSize: 5,
            search: '',
            search_type: 0,
            query: null,
            objects: [],
        }

        $timeout(function(){
            $scope.data();
        });

        $scope.setPage = function (pageNo) {
            $scope.pagination.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            $scope.data();
        };

        $scope.timer = false;
        $scope.triggerTimer =  function(){
            $scope.timer = true;
        }

        let timerStarted = false;
        $scope.doSearch = function(){
            if(!timerStarted) {
                $timeout($scope.data, 1000);
                timerStarted = true;
            }
        }

        $scope.data = function(callback = false){
            $scope.isLoading = true;
            timerStarted = false;
            let params = {
                page: $scope.pagination.currentPage,
                rows: $scope.pagination.itemsPerPage,
                search: $scope.pagination.search,
            };

            BeneficiaryRepository.get(params).then(function(response){
                let response_data = response.data;
                $scope.pagination.objects = response_data.data.rows;
                $scope.pagination.totalItems = response_data.data.total;
                $scope.isLoading = false;
                if (callback){
                    callback();
                }
            },function(){
                $scope.isLoading = false;
                if (callback){
                    callback();
                }
            });
        }

        $scope.setItemsPerPage = function(num) {
            $scope.pagination.itemsPerPage = num;
            $scope.pagination.currentPage = 1; //reset to first page
            $scope.data();
        }

        $scope.buttonNotClicked = function(){
            $scope.buttonClicked = false;
        }

        $scope.addBeneficiary = function(callback =false) {
            $scope.buttonClicked = true;
            // Add Content Group
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'modals/add-beneficiary-form.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, $timeout, $window, $sce, SwalErrorService,
                                     RegionRepository, ProvinceRepository, CityRepository, BarangayRepository,
                                     IdentificationTypeRepository, BeneficiaryTypeRepository, OccupationRepository, BeneficiaryRepository, CivilStatusRepository){

                    $scope.isProcessing = false;
                    $scope.beneficiary = {
                        first_name: '',
                        middle_name: '',
                        last_name: '',
                        suffix_name: '',
                        region_code: '',
                        province_code: '',
                        city_code: '',
                        barangay_code: '',
                        identification_type_id: '',
                        identification_number: '',
                        contact_number: '',
                        district_number: '',
                        account_type: '',
                        account_number: '',
                        beneficiary_type_id: '',
                        civil_status: '',
                        birth_date: '',
                        gender: '',
                        occupation_id: '',
                        occupation_other: '',
                        dependent: '',
                        is_interested: '',
                        skill: ''
                    };

                    $scope.regions = [];
                    $scope.provinces = [];
                    $scope.cities = [];
                    $scope.barangays = [];

                    $timeout(function(){
                        $scope.getRegions();
                        $scope.getIdentificationTypes();
                        $scope.getBeneficiaryTypes();
                        $scope.getOccupations();
                        $scope.getCivilStatuses();
                        if (callback){
                            callback();
                        }
                    });

                    $scope.isRegionLoading = false;
                    $scope.getRegions = function(){
                        $scope.isRegionLoading = true
                        RegionRepository.get().then(function(response){
                            let response_data = response.data;
                            $scope.regions = response_data.data;
                            $scope.isRegionLoading = false;
                        }, function(){
                            $scope.isRegionLoading = false;
                        });
                    }

                    $scope.isProvinceLoading = false;
                    $scope.getProvinces = function(){
                        let params = {
                            region_code: $scope.beneficiary.region_code
                        }

                        $scope.isProvinceLoading = true;
                        if ($scope.beneficiary.region_code.length <= 0){
                            $scope.provinces = [];
                            $scope.cities = [];
                            $scope.barangays = [];
                            $scope.isProvinceLoading = false;
                            return false;
                        }

                        $scope.cities = [];
                        $scope.barangays = [];
                        ProvinceRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.provinces = response_data.data;
                            $scope.isProvinceLoading = false;
                        }, function(){
                            $scope.isProvinceLoading = false;
                        });
                    }

                    $scope.isCityLoading = false;
                    $scope.getCities = function(){
                        let params = {
                            province_code: $scope.beneficiary.province_code
                        }

                        $scope.isCityLoading = true;
                        if ($scope.beneficiary.province_code.length <= 0){
                            $scope.cities = [];
                            $scope.barangays = [];
                            $scope.isCityLoading = false;
                            return false;
                        }

                        $scope.barangays = [];
                        CityRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.cities = response_data.data;
                            $scope.isCityLoading = false;
                        }, function(){
                            $scope.isCityLoading = false;
                        });
                    }

                    $scope.isBarangayLoading = false;
                    $scope.getBarangays = function(){
                        let params = {
                            city_code: $scope.beneficiary.city_code
                        }
                        $scope.isBarangayLoading = true;

                        if ($scope.beneficiary.city_code.length <= 0){
                            $scope.barangays = [];
                            $scope.isBarangayLoading = false;
                            return false;
                        }
                        $scope.barangays = [];
                        BarangayRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.barangays = response_data.data;
                            $scope.isBarangayLoading = false;
                        }, function(){
                            $scope.isBarangayLoading = false;
                        });
                    }

                    $scope.identification_types = [];
                    $scope.isIdentificationTypeLoading = true;
                    $scope.getIdentificationTypes = function(){
                        $scope.identification_types = [];
                        $scope.isIdentificationTypeLoading = true;
                        IdentificationTypeRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.identification_types = response_data.data;
                            $scope.isIdentificationTypeLoading = false;
                        }, function(){
                            $scope.isIdentificationTypeLoading = false;
                        });
                    }


                    $scope.beneficiary_types = [];
                    $scope.isBeneficiaryTypeLoading = true;
                    $scope.getBeneficiaryTypes = function(){
                        $scope.beneficiary_types = [];
                        $scope.isBeneficiaryTypeLoading = true;
                        BeneficiaryTypeRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.beneficiary_types = response_data.data;
                            $scope.isBeneficiaryTypeLoading = false;
                        }, function(){
                            $scope.isBeneficiaryTypeLoading = false;
                        });
                    }

                    $scope.civil_statuses = [];
                    $scope.isCivilStatusLoading = true;
                    $scope.getCivilStatuses = function(){
                        $scope.civil_statuses = [];
                        $scope.isCivilStatusLoading = true;
                        CivilStatusRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.civil_statuses = response_data.data;
                            $scope.isCivilStatusLoading = false;
                        }, function(){
                            $scope.isCivilStatusLoading = false;
                        });
                    }

                    $scope.occupations = [];
                    $scope.isOccupationLoading = true;
                    $scope.getOccupations = function(){
                        $scope.occupations = [];
                        $scope.isOccupationLoading = true;
                        OccupationRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.occupations = response_data.data;
                            $scope.isOccupationLoading = false;
                        }, function(){
                            $scope.isOccupationLoading = false;
                        });
                    }

                    $scope.occupation_specified = false;
                    $scope.specifiedOccupation = function(){
                        let occupation_found = false;
                        angular.forEach($scope.occupations,function(val,index){
                            if (val['id'] === $scope.beneficiary.occupation_id){
                                $scope.occupation_specified = val['specified'];
                                occupation_found = true;
                                return false;
                            }
                        });

                        if (!occupation_found){
                            $scope.occupation_specified = false;
                        }
                    }

                    $scope.isInterested = false;
                    $scope.interestedInSkillsTraining = function(){
                        if ($scope.beneficiary.is_interested === 'yes'){
                            $scope.isInterested = true;
                        }else if($scope.beneficiary.is_interested === 'no'){
                            $scope.isInterested = false;
                        }else{
                            $scope.isInterested = false;
                        }
                    }

                    $scope.isSubmitting = false;
                    $scope.submit = function(){

                        $scope.isSubmitting = true;
                        BeneficiaryRepository.store($scope.beneficiary).then(function(response){
                            let response_data = response.data;
                            Swal.fire({
                                title: 'Success',
                                icon: 'success',
                                html: response_data.message,
                                showCloseButton: true,
                                cancelButtonText: 'Close',
                            });
                            $scope.data();
                            $scope.close();
                            $scope.isSubmitting = false;
                        }, function(response){
                            $scope.isSubmitting = false;
                            SwalErrorService.errorModal(response);
                        })
                    }

                    $scope.close = function(){
                        $uibModalInstance.close(false);
                    }
                },
                size: 'xl'
            });
        }

        $scope.editBeneficiary = function(obj, callback =false) {
            $scope.buttonClicked = true;
            // Add Content Group
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'modals/edit-beneficiary-form.html',
                backdrop: 'static',
                controller: function($uibModalInstance, $scope, $timeout, $window, $sce, SwalErrorService,
                                     RegionRepository, ProvinceRepository, CityRepository, BarangayRepository,
                                     IdentificationTypeRepository, BeneficiaryTypeRepository, OccupationRepository, BeneficiaryRepository, CivilStatusRepository){

                    $scope.isProcessing = false;
                    $scope.beneficiary = {
                        first_name: obj.first_name,
                        middle_name: obj.middle_name,
                        last_name: obj.last_name,
                        suffix_name: obj.suffix_name,
                        region_code: obj.beneficiary_address.region_code,
                        province_code: obj.beneficiary_address.province_code,
                        city_code: obj.beneficiary_address.city_code,
                        barangay_code: obj.beneficiary_address.barangay_code,
                        identification_type_id: obj.beneficiary_detail.identification_type_id,
                        identification_number: obj.beneficiary_detail.identification_number,
                        contact_number: obj.beneficiary_detail.contact_number,
                        district_number: obj.beneficiary_address.district,
                        account_type: obj.beneficiary_account.account_type,
                        account_number: obj.beneficiary_account.account_number,
                        beneficiary_type_id: obj.beneficiary_type_id,
                        civil_status: obj.beneficiary_detail.civil_status,
                        birth_date: obj.beneficiary_detail.raw_birth_date,
                        gender: obj.beneficiary_detail.gender,
                        occupation_id: obj.beneficiary_detail.occupation_id,
                        occupation_other: obj.beneficiary_detail.occupation_other,
                        dependent: obj.dependent_name,
                        is_interested: obj.beneficiary_training_skill.is_interested == 1 ? 'yes' : 'no',
                        skill: obj.beneficiary_training_skill.skill
                    };

                    $scope.regions = [];
                    $scope.provinces = [];
                    $scope.cities = [];
                    $scope.barangays = [];

                    $timeout(function(){
                        $scope.getRegions(function(){
                            $scope.getProvinces(function(){
                                $scope.getCities(function(){
                                    $scope.getBarangays();
                                });
                            });
                        });
                        $scope.getIdentificationTypes();
                        $scope.getBeneficiaryTypes();
                        $scope.getOccupations($scope.specifiedOccupation);
                        $scope.getCivilStatuses();
                        $scope.interestedInSkillsTraining();
                        if (callback){
                            callback();
                        }
                    });

                    $scope.isRegionLoading = false;
                    $scope.getRegions = function(callback = false){
                        $scope.isRegionLoading = true
                        RegionRepository.get().then(function(response){
                            let response_data = response.data;
                            $scope.regions = response_data.data;
                            $scope.isRegionLoading = false;
                            if (callback){
                                callback();
                            }
                        }, function(){
                            $scope.isRegionLoading = false;
                        });
                    }

                    $scope.isProvinceLoading = false;
                    $scope.getProvinces = function(callback = false){
                        let params = {
                            region_code: $scope.beneficiary.region_code
                        }

                        $scope.isProvinceLoading = true;
                        if ($scope.beneficiary.region_code.length <= 0){
                            $scope.provinces = [];
                            $scope.cities = [];
                            $scope.barangays = [];
                            $scope.isProvinceLoading = false;
                            return false;
                        }

                        $scope.cities = [];
                        $scope.barangays = [];
                        ProvinceRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.provinces = response_data.data;
                            $scope.isProvinceLoading = false;
                            if (callback){
                                callback();
                            }
                        }, function(){
                            $scope.isProvinceLoading = false;
                        });
                    }

                    $scope.isCityLoading = false;
                    $scope.getCities = function(callback = false){
                        let params = {
                            province_code: $scope.beneficiary.province_code
                        }

                        $scope.isCityLoading = true;
                        if ($scope.beneficiary.province_code.length <= 0){
                            $scope.cities = [];
                            $scope.barangays = [];
                            $scope.isCityLoading = false;
                            return false;
                        }

                        $scope.barangays = [];
                        CityRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.cities = response_data.data;
                            $scope.isCityLoading = false;
                            if (callback){
                                callback();
                            }
                        }, function(){
                            $scope.isCityLoading = false;
                        });
                    }

                    $scope.isBarangayLoading = false;
                    $scope.getBarangays = function(callback){
                        let params = {
                            city_code: $scope.beneficiary.city_code
                        }
                        $scope.isBarangayLoading = true;

                        if ($scope.beneficiary.city_code.length <= 0){
                            $scope.barangays = [];
                            $scope.isBarangayLoading = false;
                            return false;
                        }
                        $scope.barangays = [];
                        BarangayRepository.get(params).then(function(response){
                            let response_data = response.data;
                            $scope.barangays = response_data.data;
                            $scope.isBarangayLoading = false;
                            if (callback){
                                callback();
                            }
                        }, function(){
                            $scope.isBarangayLoading = false;
                        });
                    }

                    $scope.identification_types = [];
                    $scope.isIdentificationTypeLoading = true;
                    $scope.getIdentificationTypes = function(){
                        $scope.identification_types = [];
                        $scope.isIdentificationTypeLoading = true;
                        IdentificationTypeRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.identification_types = response_data.data;
                            $scope.isIdentificationTypeLoading = false;
                        }, function(){
                            $scope.isIdentificationTypeLoading = false;
                        });
                    }


                    $scope.beneficiary_types = [];
                    $scope.isBeneficiaryTypeLoading = true;
                    $scope.getBeneficiaryTypes = function(){
                        $scope.beneficiary_types = [];
                        $scope.isBeneficiaryTypeLoading = true;
                        BeneficiaryTypeRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.beneficiary_types = response_data.data;
                            $scope.isBeneficiaryTypeLoading = false;
                        }, function(){
                            $scope.isBeneficiaryTypeLoading = false;
                        });
                    }

                    $scope.civil_statuses = [];
                    $scope.isCivilStatusLoading = true;
                    $scope.getCivilStatuses = function(){
                        $scope.civil_statuses = [];
                        $scope.isCivilStatusLoading = true;
                        CivilStatusRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.civil_statuses = response_data.data;
                            $scope.isCivilStatusLoading = false;
                        }, function(){
                            $scope.isCivilStatusLoading = false;
                        });
                    }

                    $scope.occupations = [];
                    $scope.isOccupationLoading = true;
                    $scope.getOccupations = function(callback = false){
                        $scope.occupations = [];
                        $scope.isOccupationLoading = true;
                        OccupationRepository.get({}).then(function(response){
                            let response_data = response.data;
                            $scope.occupations = response_data.data;
                            $scope.isOccupationLoading = false;
                            if (callback){
                                callback();
                            }
                        }, function(){
                            $scope.isOccupationLoading = false;
                        });
                    }

                    $scope.occupation_specified = false;
                    $scope.specifiedOccupation = function(){
                        let occupation_found = false;
                        angular.forEach($scope.occupations,function(val,index){
                            if (val['id'] === $scope.beneficiary.occupation_id){
                                $scope.occupation_specified = val['specified'];
                                occupation_found = true;
                                return false;
                            }
                        });

                        if (!occupation_found){
                            $scope.occupation_specified = false;
                        }
                    }

                    $scope.isInterested = false;
                    $scope.interestedInSkillsTraining = function(){
                        if ($scope.beneficiary.is_interested === 'yes'){
                            $scope.isInterested = true;
                        }else if($scope.beneficiary.is_interested === 'no'){
                            $scope.isInterested = false;
                        }else{
                            $scope.isInterested = false;
                        }
                    }

                    $scope.isSubmitting = false;
                    $scope.submit = function(){
                        $scope.isSubmitting = true;
                        BeneficiaryRepository.patch(obj.id, $scope.beneficiary).then(function(response){
                            let response_data = response.data;
                            Swal.fire({
                                title: 'Success',
                                icon: 'success',
                                html: response_data.message,
                                showCloseButton: true,
                                cancelButtonText: 'Close',
                            });
                            $scope.data();
                            $scope.close();
                            $scope.isSubmitting = false;
                        }, function(response){
                            $scope.isSubmitting = false;
                            SwalErrorService.errorModal(response);
                        })
                    }

                    $scope.close = function(){
                        $uibModalInstance.close(false);
                    }
                },
                size: 'xl'
            });
        }

        $scope.delete = function (id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    BeneficiaryRepository.delete(id).then(function(response){
                        let response_data = response.data;
                        Swal.fire({
                            title: 'Success',
                            icon: 'success',
                            html: response_data.message,
                            showCloseButton: true,
                            cancelButtonText: 'Close',
                        });
                        $scope.data();
                    }, function(response){
                        SwalErrorService.errorModal(response);
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire(
                        'Cancelled',
                        'Your beneficiary is safe',
                        'error'
                    )
                }
            })
        }
    }


})();
