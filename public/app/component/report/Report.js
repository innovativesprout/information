(function(){
    "use strict";

    angular.module('Report', ['ui.bootstrap', 'AngularVersioning', 'SwalError',
        'RegionRepository','ProvinceRepository','CityRepository','BarangayRepository', 'IdentificationTypeRepository',
        'BeneficiaryTypeRepository', 'OccupationRepository', 'BeneficiaryRepository', 'ReportRepository', 'SettingMetaRepository',
        'ui.mask'])

    .config(function($interpolateProvider, $httpProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
        $httpProvider.interceptors.push('preventTemplateCache');
    })

    .constant('BASE', {
        'API_URL': '/',
        'ASSETS_URL': 'app/component/report/templates/',
    })

    .directive('report', Report)

    function Report(BASE) {

        return {
            restrict: 'EA',
            controller: BeneficiaryController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'index.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function BeneficiaryController($scope, $window, $timeout, $filter, $uibModal, BASE,
                                   RegionRepository, ProvinceRepository, CityRepository, BarangayRepository,
                                   BeneficiaryTypeRepository, ReportRepository, SettingMetaRepository, SwalErrorService) {

        $scope.isLoading = false;

        $timeout(function(){
            $scope.getRegions();
            $scope.getBeneficiaryTypes();
            $scope.getSettings();
        });

        $scope.beneficiary = {
            region_code: '',
            province_code: '',
            city_code: '',
            barangay_code: '',
            beneficiary_type_id: '',
            civil_status: '',
            gender: '',
        };

        $scope.report = {
            project_name: '',
            dole_regional_office: '',
            footer: '',
            sub_footer: '',
            signature_label: '',
            signature_title: '',
            signature_sub_title: '',
        }

        $scope.isRetrievingSetting = true;
        $scope.getSettings = function(){
            $scope.isRetrievingSetting = true;
            SettingMetaRepository.get({}).then(function(response){
                $scope.report = response.data.data;
                $scope.isRetrievingSetting = false;
            }, function(){
                $scope.isRetrievingSetting = false;
            })
        }

        $scope.regions = [];
        $scope.getRegions = function(){
            RegionRepository.get().then(function(response){
                let response_data = response.data;
                $scope.regions = response_data.data;
            })
        }

        $scope.isProvinceLoading = true;
        $scope.getProvinces = function(){
            let params = {
                region_code: $scope.beneficiary.region_code
            }

            $scope.isProvinceLoading = true;
            if ($scope.beneficiary.region_code.length <= 0){
                $scope.provinces = [];
                $scope.cities = [];
                $scope.barangays = [];
                return false;
            }

            // Assign a value into the report object for dole regional office
            angular.forEach($scope.regions, function(val, index){
                if (val['region_code'] === $scope.beneficiary.region_code){
                    $scope.report.dole_regional_office = val['name'];
                    return false;
                }
            });

            $scope.cities = [];
            $scope.barangays = [];
            ProvinceRepository.get(params).then(function(response){
                let response_data = response.data;
                $scope.provinces = response_data.data;
                $scope.isProvinceLoading = false;
            }, function(){
                $scope.isProvinceLoading = false;
            });
        }

        $scope.isCityLoading = true;
        $scope.getCities = function(){
            let params = {
                province_code: $scope.beneficiary.province_code
            }

            $scope.isCityLoading = true;
            if ($scope.beneficiary.province_code.length <= 0){
                $scope.cities = [];
                $scope.barangays = [];
                return false;
            }

            $scope.barangays = [];
            CityRepository.get(params).then(function(response){
                let response_data = response.data;
                $scope.cities = response_data.data;
                $scope.isCityLoading = false;
            }, function(){
                $scope.isCityLoading = false;
            });
        }

        $scope.isBarangayLoading = true;
        $scope.getBarangays = function(){
            let params = {
                city_code: $scope.beneficiary.city_code
            }
            $scope.isBarangayLoading = true;

            if ($scope.beneficiary.city_code.length <= 0){
                $scope.barangays = [];
                return false;
            }
            $scope.barangays = [];
            BarangayRepository.get(params).then(function(response){
                let response_data = response.data;
                $scope.barangays = response_data.data;
                $scope.isBarangayLoading = false;
            }, function(){
                $scope.isBarangayLoading = false;
            });
        }
        $scope.beneficiary_types = [];
        $scope.isBeneficiaryTypeLoading = true;
        $scope.getBeneficiaryTypes = function(){
            $scope.beneficiary_types = [];
            $scope.isBeneficiaryTypeLoading = true;
            BeneficiaryTypeRepository.get({}).then(function(response){
                let response_data = response.data;
                $scope.beneficiary_types = response_data.data;
                $scope.isBeneficiaryTypeLoading = false;
            }, function(){
                $scope.isBeneficiaryTypeLoading = false;
            });
        }

        $scope.results = [];
        $scope.generate = function(){
            ReportRepository.generate($scope.beneficiary).then(function(response){

            });
        }

        $scope.isExporting = false;
        $scope.export = function(){
            let params = {
                report: $scope.report,
                beneficiary: $scope.beneficiary
            }
            $scope.isExporting = true;
            ReportRepository.export(params).then(function(data, status, headers){
                $window.location.href = data.data;
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    html: "Export has been successful!",
                    showCloseButton: true,
                    cancelButtonText: 'Close',
                });
                $scope.isExporting = false;
            }, function(response){
                SwalErrorService.errorModal(response);
                $scope.isExporting = false;
            });
        }

    }

    /**
     * View Order Function
     */
    function ViewOrder(BASE) {

        return {
            restrict: 'EA',
            scope: {
              orderId: '@'
            },
            controller: ViewOrderController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'view-order.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function ViewOrderController($scope, $window, $timeout, $filter, $uibModal, BASE, OrderRepository) {

        $scope.order = null;

        $scope.isOrderFetching = false;

        $timeout(function(){
            let param = {
                order_id: $scope.orderId
            };
            $scope.isOrderFetching = true;
            OrderRepository.get(param).then(function(response){
                $scope.order = response.data.data;
                $scope.isOrderFetching = false;
            },function(){
                $scope.order = null;
                $scope.isOrderFetching = false;
            });

        });
    }

})();
