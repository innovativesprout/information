(function(){
    "use strict";

    angular.module('ReportSetting', ['ui.bootstrap', 'AngularVersioning', 'SwalError', 'SettingMetaRepository'])

    .config(function($interpolateProvider, $httpProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
        $httpProvider.interceptors.push('preventTemplateCache');
    })

    .constant('BASE', {
        'API_URL': '/',
        'ASSETS_URL': 'app/component/settings/report_setting/templates/',
    })

    .directive('reportSetting', ReportSetting)

    function ReportSetting(BASE) {

        return {
            restrict: 'EA',
            controller: ReportSettingController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'index.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function ReportSettingController($scope, $window, $timeout, $filter, $uibModal, BASE,
                                     SettingMetaRepository, SwalErrorService) {
        $timeout(function(){
            $scope.getSettings();
        });

        $scope.report = {
            project_name: '',
            dole_regional_office: '',
            footer: '',
            sub_footer: '',
            signature_label: '',
            signature_title: '',
            signature_sub_title: '',
        }

        $scope.isRetrievingSetting = true;
        $scope.getSettings = function(){
            $scope.isRetrievingSetting = true;
            SettingMetaRepository.get({}).then(function(response){
                $scope.report = response.data.data;
                $scope.isRetrievingSetting = false;
            }, function(){
                $scope.isRetrievingSetting = false;
            })
        }

        $scope.isLoading = false;
        $scope.submit = function(){
            $scope.isLoading = true;
            SettingMetaRepository.store($scope.report).then(function(response){
                let response_data = response.data;
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    html: response_data.message,
                    showCloseButton: true,
                    cancelButtonText: 'Close',
                });
                $scope.getSettings();
                $scope.isLoading = false;
            }, function(response){
                SwalErrorService.errorModal(response);
                $scope.isLoading = false;
            })
        }

    }

    /**
     * View Order Function
     */
    function ViewOrder(BASE) {

        return {
            restrict: 'EA',
            scope: {
              orderId: '@'
            },
            controller: ViewOrderController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'view-order.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function ViewOrderController($scope, $window, $timeout, $filter, $uibModal, BASE, OrderRepository) {

        $scope.order = null;

        $scope.isOrderFetching = false;

        $timeout(function(){
            let param = {
                order_id: $scope.orderId
            };
            $scope.isOrderFetching = true;
            OrderRepository.get(param).then(function(response){
                $scope.order = response.data.data;
                $scope.isOrderFetching = false;
            },function(){
                $scope.order = null;
                $scope.isOrderFetching = false;
            });

        });
    }

})();
