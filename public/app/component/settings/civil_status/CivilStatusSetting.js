(function(){
    "use strict";

    angular.module('CivilStatusSetting', ['ui.bootstrap', 'AngularVersioning', 'SwalError', 'CivilStatusRepository'])

    .config(function($interpolateProvider, $httpProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
        $httpProvider.interceptors.push('preventTemplateCache');
    })

    .constant('BASE', {
        'API_URL': '/',
        'ASSETS_URL': 'app/component/settings/civil_status/templates/',
    })

    .directive('civilStatusSetting', CivilStatusSetting)

    function CivilStatusSetting(BASE) {

        return {
            restrict: 'EA',
            controller: CivilStatusSettingController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'index.html',
            link: function (scope, element, attributes) {

            }
        }

    }

    function CivilStatusSettingController($scope, $window, $timeout, $filter, $uibModal, BASE,
                                              CivilStatusRepository, SwalErrorService) {
        $scope.pagination = {
            totalItems: 0,
            currentPage: 1,
            itemsPerPage: 10,
            maxSize: 5,
            search: '',
            objects: [],
        }

        $timeout(function(){
            $scope.data();
        });

        $scope.setPage = function (pageNo) {
            $scope.pagination.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            $scope.data();
        };

        $scope.timer = false;
        $scope.triggerTimer =  function(){
            $scope.timer = true;
        }

        let timerStarted = false;
        $scope.doSearch = function(){
            if(!timerStarted) {
                $timeout($scope.data, 1000);
                timerStarted = true;
            }
        }

        $scope.data = function(callback = false){
            $scope.isLoading = true;
            timerStarted = false;
            $scope.pagination.objects = [];
            let params = {
                page: $scope.pagination.currentPage,
                rows: $scope.pagination.itemsPerPage,
                search: $scope.pagination.search,
            };

            CivilStatusRepository.paginated(params).then(function(response){
                let response_data = response.data;
                $scope.pagination.objects = response_data.data.rows;
                $scope.pagination.totalItems = response_data.data.total;
                $scope.isLoading = false;
                if (callback){
                    callback();
                }
            },function(){
                $scope.isLoading = false;
                if (callback){
                    callback();
                }
            });
        }

        $scope.setItemsPerPage = function(num) {
            $scope.pagination.itemsPerPage = num;
            $scope.pagination.currentPage = 1; //reset to first page
            $scope.data();
        }

        $scope.buttonClicked = false;
        $scope.buttonNotClicked = function(){
            $scope.buttonClicked = false;
        }

        $scope.add = function(callback=false) {
            $scope.buttonClicked = true;
            // Add Content Group
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'modals/add-form.html',
                backdrop: 'static',
                keyboard : false,
                controller: function($uibModalInstance, $scope, $timeout, $window, $sce, SwalErrorService,
                                     CivilStatusRepository){

                    $scope.isProcessing = false;
                    $scope.form = {
                        name: ''
                    };

                    $scope.isSubmitting = false;
                    $scope.submit = function(){

                        $scope.isSubmitting = true;
                        CivilStatusRepository.store($scope.form).then(function(response){
                            let response_data = response.data;
                            Swal.fire({
                                title: 'Success',
                                icon: 'success',
                                html: response_data.message,
                                showCloseButton: true,
                                cancelButtonText: 'Close',
                            });

                            $scope.data();
                            $scope.close();
                            $scope.isSubmitting = false;
                            if (callback){
                                callback();
                            }
                        }, function(response){

                            if (callback){
                                callback();
                            }
                            $scope.isSubmitting = false;
                            SwalErrorService.errorModal(response);
                        })
                    }

                    $scope.close = function(){
                        if (callback){
                            callback();
                        }
                        $uibModalInstance.close(false);
                    }
                },
                size: 'sm'
            });
        }

        $scope.edit = function(obj, callback=false) {
            $scope.buttonClicked = true;
            // Add Content Group
            $uibModal.open({
                animation: true,
                templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'modals/edit-form.html',
                backdrop: 'static',
                keyboard : false,
                controller: function($uibModalInstance, $scope, $timeout, $window, $sce, SwalErrorService,
                                     CivilStatusRepository){

                    $scope.isProcessing = false;
                    $scope.form = obj;

                    $scope.isSubmitting = false;
                    $scope.submit = function(){

                        $scope.isSubmitting = true;
                        CivilStatusRepository.patch(obj.id, $scope.form).then(function(response){
                            let response_data = response.data;
                            Swal.fire({
                                title: 'Success',
                                icon: 'success',
                                html: response_data.message,
                                showCloseButton: true,
                                cancelButtonText: 'Close',
                            });

                            $scope.data();
                            $scope.close();
                            $scope.isSubmitting = false;
                            if (callback){
                                callback();
                            }
                        }, function(response){

                            if (callback){
                                callback();
                            }
                            $scope.isSubmitting = false;
                            SwalErrorService.errorModal(response);
                        })
                    }

                    $scope.close = function(){
                        if (callback){
                            callback();
                        }
                        $uibModalInstance.close(false);
                    }
                },
                size: 'sm'
            });
        }

        $scope.delete = function (id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    CivilStatusRepository.destroy(id).then(function(response){
                        let response_data = response.data;
                        Swal.fire({
                            title: 'Success',
                            icon: 'success',
                            html: response_data.message,
                            showCloseButton: true,
                            cancelButtonText: 'Close',
                        });
                        $scope.data();
                    }, function(response){
                        SwalErrorService.errorModal(response);
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire(
                        'Cancelled',
                        'Your civil status type is safe',
                        'error'
                    )
                }
            })
        }
    }
})();
