(function(){
    "use strict";

    angular.module('IdentificationTypeRepository', [])
        .constant('IDENTIFICATION_TYPE_BASE', {
            'API_URL': '/',
        })
        .factory('IdentificationTypeRepository', IdentificationTypeRepository)

    function IdentificationTypeRepository(IDENTIFICATION_TYPE_BASE, $http) {

        let api_url = IDENTIFICATION_TYPE_BASE.API_URL + 'api/v1/identification-types';
        let resource = {};

        resource.paginated = function(params){
            return $http.get(api_url + '?paginated', {params});
        }

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.store = function(params){
            return $http.post(api_url, params);
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        resource.destroy = function(id){
            return $http.delete(api_url + '/' + id);
        }

        return resource;
    }
})();
