(function(){
    "use strict";

    angular.module('SettingMetaRepository', [])
        .constant('SETTING_META_BASE', {
            'API_URL': '/',
        })
        .factory('SettingMetaRepository', SettingMetaRepository)

    function SettingMetaRepository(SETTING_META_BASE, $http) {

        let api_url = SETTING_META_BASE.API_URL + 'api/v1/setting-metas';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.store = function(params){
            return $http.post(api_url, params);
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        return resource;
    }
})();
