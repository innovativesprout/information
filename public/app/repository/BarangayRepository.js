(function(){
    "use strict";

    angular.module('BarangayRepository', [])
        .constant('BARANGAY_BASE', {
            'API_URL': '/',
        })
        .factory('BarangayRepository', BarangayRepository)

    function BarangayRepository(BARANGAY_BASE, $http) {

        let api_url = BARANGAY_BASE.API_URL + 'api/v1/barangays';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        return resource;
    }
})();
