(function(){
    "use strict";

    angular.module('CityRepository', [])
        .constant('CITY_BASE', {
            'API_URL': '/',
        })
        .factory('CityRepository', CityRepository)

    function CityRepository(CITY_BASE, $http) {

        let api_url = CITY_BASE.API_URL + 'api/v1/cities';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        return resource;
    }
})();
