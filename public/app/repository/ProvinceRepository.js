(function(){
    "use strict";

    angular.module('ProvinceRepository', [])
        .constant('PROVINCE_BASE', {
            'API_URL': '/',
        })
        .factory('ProvinceRepository', ProvinceRepository)

    function ProvinceRepository(PROVINCE_BASE, $http) {

        let api_url = PROVINCE_BASE.API_URL + 'api/v1/provinces';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        return resource;
    }
})();
