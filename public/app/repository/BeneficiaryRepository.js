(function(){
    "use strict";

    angular.module('BeneficiaryRepository', [])
        .constant('BENEFICIARY_BASE', {
            'API_URL': '/',
        })
        .factory('BeneficiaryRepository', BeneficiaryRepository)

    function BeneficiaryRepository(BENEFICIARY_BASE, $http) {

        let api_url = BENEFICIARY_BASE.API_URL + 'api/v1/beneficiaries';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.store = function(params){
            return $http.post(api_url, params);
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        resource.delete = function(id){
            return $http.delete(api_url + '/' + id);
        }

        return resource;
    }
})();
