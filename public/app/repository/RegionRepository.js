(function(){
    "use strict";

    angular.module('RegionRepository', [])
        .constant('REGION_BASE', {
            'API_URL': '/',
        })
        .factory('RegionRepository', RegionRepository)

    function RegionRepository(REGION_BASE, $http) {

        let api_url = REGION_BASE.API_URL + 'api/v1/regions';
        let resource = {};

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        return resource;
    }
})();
