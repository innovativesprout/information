(function(){
    "use strict";

    angular.module('OrderRepository', [])
        .constant('BASE', {
            'API_URL': '/',
        })
        .factory('OrderRepository', OrderRepository)

    function OrderRepository(BASE, $http) {

        let api_url = BASE.API_URL + 'admin/api/v1/orders';
        let repo = {};

        repo.all = function(params){
            return $http.get(api_url, {params});
        }

        repo.get = function(params){
            return $http.get(api_url + "/" + params.order_id);
        }

        return repo;
    }
})();
