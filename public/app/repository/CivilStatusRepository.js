(function(){
    "use strict";

    angular.module('CivilStatusRepository', [])
        .constant('CIVIL_STATUS_BASE', {
            'API_URL': '/',
        })
        .factory('CivilStatusRepository', CivilStatusRepository)

    function CivilStatusRepository(CIVIL_STATUS_BASE, $http) {

        let api_url = CIVIL_STATUS_BASE.API_URL + 'api/v1/civil-statuses';
        let resource = {};

        resource.paginated = function(params){
            return $http.get(api_url + '?paginated', {params});
        }

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.store = function(params){
            return $http.post(api_url, params);
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        resource.destroy = function(id){
            return $http.delete(api_url + '/' + id);
        }

        return resource;
    }
})();
