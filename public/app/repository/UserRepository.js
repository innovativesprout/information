(function(){
    "use strict";

    angular.module('UserRepository', [])
        .constant('USER_BASE', {
            'API_URL': '/',
        })
        .factory('UserRepository', UserRepository)

    function UserRepository(USER_BASE, $http) {

        let api_url = USER_BASE.API_URL + 'api/v1/users';
        let resource = {};

        resource.paginated = function(params){
            return $http.get(api_url + '?paginated', {params});
        }

        resource.get = function(params){
            return $http.get(api_url, {params});
        }

        resource.show = function(id, params){
            return $http.get(api_url + '/' + id, {params});
        }

        resource.store = function(params){
            return $http.post(api_url, params);
        }

        resource.change_password = function(params){
            return $http.post(api_url + '/change-password', params);
        }

        resource.patch = function(id, params){
            return $http.patch(api_url + '/' + id, params);
        }

        resource.destroy = function(id){
            return $http.delete(api_url + '/' + id);
        }

        return resource;
    }
})();
