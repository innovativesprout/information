(function(){
    "use strict";

    angular.module('ReportRepository', [])
        .constant('REPORT_BASE', {
            'API_URL': '/',
        })
        .factory('ReportRepository', ReportRepository)

    function ReportRepository(REPORT_BASE, $http) {

        let api_url = REPORT_BASE.API_URL + 'api/v1/reports';
        let resource = {};

        resource.generate = function(params){
            return $http.post(api_url, params);
        }

        resource.export = function(params){
            return $http.post(api_url + '/export', params);
        }

        return resource;
    }
})();
