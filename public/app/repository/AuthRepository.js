(function(){
    "use strict";

    angular.module('AuthRepository', [])
        .constant('BASE', {
            'API_URL': '/',
        })
        .factory('AuthRepository', AuthRepository)

    function AuthRepository(BASE, $http) {

        let api_url = BASE.API_URL + 'api/v1/auth';
        let repo = {};

        repo.logout = function(){
            return $http.post(api_url + '/logout');
        }

        repo.validate = function(params){
            return $http.post(api_url + '/validate', params);
        }

        repo.login = function(params){
            return $http.post(api_url + '/login', params);
        }

        repo.isLoggedIn = function(){
            return $http.get(api_url + '/is-logged-in');
        }

        repo.register = function(params){
            return $http.post(BASE.API_URL + 'register/store', params);
        }

        repo.register_using_form = function(params){
            return $http.post(BASE.API_URL + 'register/store', params);
        }

        repo.forgot = function(params){
            return $http.post(BASE.API_URL + 'forgot-password', params);
        }

        repo.reset = function(params){
            return $http.post(BASE.API_URL + 'password/reset', params);
        }

        return repo;
    }
})();
