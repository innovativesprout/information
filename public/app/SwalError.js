(function(){
    "use strict";

    angular.module('SwalError', [])
        .constant('BASE', {
            'API_URL': '/',
        })
        .service('SwalErrorService', SwalErrorService)

    function SwalErrorService() {

        this.errorModal = function(response){
            let response_error = response.data;
            let html = '';
            if (!response_error.data) {
                html += response_error.message;
            } else if (typeof response_error.data === 'string' ) {
                html += response_error.message;
            } else {
                let errors = response_error.data;
                html += '<ul style=" list-style: none; margin: 0; padding: 0;">';
                angular.forEach(errors, function (values, index) {
                    angular.forEach(values, function (value, index) {
                        html += '<li>' + value[0] + '</li>';
                    });
                });
                html += '</ul>';
            }

            Swal.fire(
                'Failed!',
                html,
                'error'
            );
        }
    }
})();
