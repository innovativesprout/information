<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class SettingMeta extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $resource = new \App\Models\SettingMeta;
        $resource = $resource->get();

        $mapped = [];
        foreach ($resource as $item) {
            $mapped[$item['meta_key']] = $item['meta_value'];
        }
        return $this->respondSuccessful('Success', $mapped);
    }

    public function store(Request $request)
    {
        try {
            if (count($request->toArray()) > 0){
                foreach ($request->all() as $index => $item) {
                    \App\Models\SettingMeta::updateOrCreate(['meta_key' => $index], ['meta_key' => $index, 'meta_value' => $item]);
                }
            }

            return $this->respondSuccessful('Report settings have been successfully updated.', []);
        } catch(\Exception $e){
            return $this->respondInternalServerError('There is an error. Please try again later.', []);
        }
    }
}
