<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class City extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $resource = new \App\Models\City;
        if ($request->has('city_code')){
            $resource = $resource->ByCityCode($request->city_code);
        }

        if ($request->has('province_code')){
            $resource = $resource->ByProvinceCode($request->province_code);
        }

        $resource = $resource->get();

        return $this->respondSuccessful('Success', $resource);
    }
}
