<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class Province extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $resource = new \App\Models\Province;

        if ($request->has('province_code')){
            $resource = $resource->ByProvinceCode($request->province_code);
        }

        if ($request->has('region_code')){
            $resource = $resource->ByRegionCode($request->region_code);
        }

        $resource = $resource->get();

        return $this->respondSuccessful('Success', $resource);
    }
}
