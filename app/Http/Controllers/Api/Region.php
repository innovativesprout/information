<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class Region extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $resource = new \App\Models\Region();

        if ($request->has('region_code')){
            $resource = $resource->ByRegionCode($request->region_code);
        }

        $resource = $resource->get();

        return $this->respondSuccessful('Success', $resource);
    }
}
