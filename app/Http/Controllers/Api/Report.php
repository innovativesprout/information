<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class Report extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $resource = new \App\Models\Beneficiary();
        $resource = $resource->with(['beneficiary_account', 'beneficiary_address', 'beneficiary_training_skill', 'beneficiary_detail']);

        if ($request->has('region_code') && !empty($request->region_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('region_code', $request->region_code);
            });
        }

        if ($request->has('province_code') && !empty($request->province_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('province_code', $request->province_code);
            });
        }

        if ($request->has('city_code') && !empty($request->city_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('city_code', $request->city_code);
            });
        }

        if ($request->has('barangay_code') && !empty($request->barangay_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('barangay_code', $request->barangay_code);
            });
        }

        if ($request->has('beneficiary_type_id') && !empty($request->beneficiary_type_id)){
            $resource = $resource->where('beneficiary_type_id', $request->beneficiary_type_id);
        }

        if ($request->has('civil_status') && !empty($request->civil_status)){
            $resource = $resource->whereHas('beneficiary_detail', function($query) use ($request){
                $query->where('civil_status', $request->civil_status);
            });
        }

        if ($request->has('gender') && !empty($request->gender)){
            $resource = $resource->whereHas('beneficiary_detail', function($query) use ($request){
                $query->where('gender', $request->gender);
            });
        }

        $resource = $resource->get();

        return $this->respondSuccessful('Success', $resource);
    }

    public function export(Request $request)
    {
        $report = $request->report;
        $beneficiary = $request->beneficiary;
        $request = (new Request);
        $request->setMethod('POST');
        $request->request->add($beneficiary);

        $resource = new \App\Models\Beneficiary();
        $resource = $resource->with(['beneficiary_account', 'beneficiary_address', 'beneficiary_training_skill', 'beneficiary_detail']);

        if ($request->has('region_code') && !empty($request->region_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('region_code', $request->region_code);
            });
        }

        if ($request->has('province_code') && !empty($request->province_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('province_code', $request->province_code);
            });
        }

        if ($request->has('city_code') && !empty($request->city_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('city_code', $request->city_code);
            });
        }

        if ($request->has('barangay_code') && !empty($request->barangay_code)){
            $resource = $resource->whereHas('beneficiary_address', function($query) use ($request){
                $query->where('barangay_code', $request->barangay_code);
            });
        }

        if ($request->has('beneficiary_type_id') && !empty($request->beneficiary_type_id)){
            $resource = $resource->where('beneficiary_type_id', $request->beneficiary_type_id);
        }

        if ($request->has('civil_status') && !empty($request->civil_status)){
            $resource = $resource->whereHas('beneficiary_detail', function($query) use ($request){
                $query->where('civil_status', $request->civil_status);
            });
        }

        if ($request->has('gender') && !empty($request->gender)){
            $resource = $resource->whereHas('beneficiary_detail', function($query) use ($request){
                $query->where('gender', $request->gender);
            });
        }

        $resource = $resource->get();

        $province_found = \App\Models\Province::where('province_code', $beneficiary['province_code'])->first();
        $beneficiary['province_name'] = $province_found ? $province_found->name : '';

        $city_found = \App\Models\City::where('city_code', $beneficiary['city_code'])->first();
        $beneficiary['city_name'] = $city_found ? $city_found->name : '';

        $barangay_found = \App\Models\Barangay::where('barangay_code', $beneficiary['barangay_code'])->first();
        $beneficiary['barangay_name'] = $barangay_found ? $barangay_found->name : '';

        return (new \App\Services\Report\Report)->handle($beneficiary, $report, $resource);
    }
}
