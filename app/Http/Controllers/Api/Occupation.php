<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\DataExistsException;
use App\Exceptions\InvalidInputException;
use App\Validators\UpdateValidators\OccupationStoreValidator;
use App\Validators\UpdateValidators\OccupationUpdateValidator;
use Illuminate\Http\Request;

class Occupation extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $resource = new \App\Models\Occupation();
        if ($request->has('paginated')){
            $search   = $request->has('search') ? $request->search : '';
            $page     = $request->has('page') ? $request->page : 1;
            $rows     = $request->has('rows') ? $request->rows : 10;
            $offset   = ($page - 1) * $rows;

            if ($search != ''){
                $resource = $resource->where(function($query) use ($search){
                    $query->where('slug', 'LIKE', '%' . $search .'%')
                        ->orWhere('name', 'LIKE', '%' . $search .'%');
                });
            }
            $total = $resource->count();
            $resource = $resource->skip($offset)->take($rows);
            $resource = $resource->get()->map(function(&$val){
                $val['specified'] = (bool) $val['specified'];
                return $val;
            });

            $response = ['rows' => $resource, 'total' => $total];
        }else{
            $response = $resource->get()->map(function(&$val){
                $val['specified'] = (bool) $val['specified'];
                return $val;
            });;
        }

        return $this->respondSuccessful('Successful', $response);
    }

    public function store(Request $request)
    {
        $validator = new OccupationStoreValidator();

        try {
            $validator->handle($request->toArray());

            $resource = new \App\Models\Occupation();
            $resource->slug = $request->name;
            $resource->name = $request->name;
            $resource->specified = $request->specified;
            $resource->save();

            return $this->respondSuccessful('New occupation type has been added.');
        } catch (InvalidInputException $e) {
            return $this->respondUnprocessable('There is an empty field.', $validator->getErrors());
        } catch (DataExistsException $e) {
            return $this->respondUnprocessable('Data is already exist.', $validator->getErrors());
        } catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }

    public function update(Request $request, $id)
    {
        $validator = new OccupationUpdateValidator();

        try {
            $validator->handle($request->toArray());

            $resource = (new \App\Models\Occupation())->find($id);
            $resource->slug = $request->name;
            $resource->name = $request->name;
            $resource->specified = $request->specified;
            $resource->save();

            return $this->respondSuccessful('Occupation type has been updated.');
        } catch (InvalidInputException $e) {
            return $this->respondUnprocessable('There is an empty field.', $validator->getErrors());
        } catch (DataExistsException $e) {
            return $this->respondUnprocessable('Data is already exist.', $validator->getErrors());
        } catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }

    public function destroy($id)
    {
        try {
            $resource = new \App\Models\Occupation();
            $resource = $resource->find($id);
            $resource->delete();
            return $this->respondSuccessful('Occupation type has been deleted.');
        }  catch (\Exception $e) {
            return $this->respondSuccessful('There is an error. Please try again.');
        }
    }
}
