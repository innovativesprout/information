<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\DataExistsException;
use App\Exceptions\InvalidInputException;
use App\Validators\UpdateValidators\UserPasswordUpdateValidator;
use App\Validators\StoreValidators\UserStoreValidator;
use App\Validators\UpdateValidators\UserUpdateValidator;
use Illuminate\Http\Request;

class User extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $resource = new \App\Models\User();
        if ($request->has('paginated')){
            $search   = $request->has('search') ? $request->search : '';
            $page     = $request->has('page') ? $request->page : 1;
            $rows     = $request->has('rows') ? $request->rows : 10;
            $offset   = ($page - 1) * $rows;

            if ($search != ''){
                $resource = $resource->where(function($query) use ($search){
                    $query->where('slug', 'LIKE', '%' . $search .'%')
                        ->orWhere('name', 'LIKE', '%' . $search .'%');
                });
            }
            $total = $resource->count();
            $resource = $resource->skip($offset)->take($rows);
            $resource = $resource->get();

            $response = ['rows' => $resource, 'total' => $total];
        }else{
            $response = $resource->get();
        }

        return $this->respondSuccessful('Successful', $response);
    }

    public function store(Request $request)
    {
        $validator = new UserStoreValidator;

        try {
            $validator->handle($request->toArray());

            $resource = new \App\Models\User();
            $resource->name = $request->name;
            $resource->email = $request->email;
            $resource->password = bcrypt($request->password);
            $resource->save();

            return $this->respondSuccessful('New user has been added.');
        } catch (InvalidInputException $e) {
            return $this->respondUnprocessable('There is an empty field.', $validator->getErrors());
        } catch (DataExistsException $e) {
            return $this->respondUnprocessable('Data is already exist.', $validator->getErrors());
        } catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }

    public function update(Request $request, $id)
    {
        $validator = new UserUpdateValidator();

        try {
            $validator->handle($request->toArray());

            $resource = (new \App\Models\User())->find($id);
            $resource->name = $request->name;
            $resource->email = $request->email;
            $resource->save();

            return $this->respondSuccessful('User has been updated.');
        } catch (InvalidInputException $e) {
            return $this->respondUnprocessable('There is an empty field.', $validator->getErrors());
        } catch (DataExistsException $e) {
            return $this->respondUnprocessable('Data is already exist.', $validator->getErrors());
        } catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }

    public function changePassword(Request $request)
    {
        $validator = new UserPasswordUpdateValidator;

        try {
            $validator->handle($request->toArray());

            $resource = (new \App\Models\User())->find($request->id);
            $resource->password = bcrypt($request->password);
            $resource->save();

            return $this->respondSuccessful('User password has been updated.');
        } catch (InvalidInputException $e) {
            return $this->respondUnprocessable('There is an empty field.', $validator->getErrors());
        } catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }

    public function destroy($id)
    {

        try {

            $resource = new \App\Models\User();
            $resource = $resource->find($id);
            $resource->delete();

            return $this->respondSuccessful('User has been deleted.');
        }  catch (\Exception $e) {
            return $this->respondInternalServerError('There is an error. Please try again.');
        }
    }
}
