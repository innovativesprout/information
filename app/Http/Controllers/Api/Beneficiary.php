<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InvalidInputException;
use App\Validators\UpdateValidators\BeneficiaryStoreValidator;
use App\Validators\UpdateValidators\BeneficiaryUpdateValidator;
use DB;
use Illuminate\Http\Request;

class Beneficiary extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        try{
            $search   = $request->has('search') ? $request->search : '';
            $page     = $request->has('page') ? $request->page : 1;
            $rows     = $request->has('rows') ? $request->rows : 10;
            $offset   = ($page - 1) * $rows;

            $resource = new \App\Models\Beneficiary();
            if ($search != ''){
                $resource = $resource->where(function($query) use ($search){
                    $query->where('last_name', 'LIKE', '%' . $search .'%')
                        ->orWhere('middle_name', 'LIKE', '%' . $search .'%')
                        ->orWhere('first_name', 'LIKE', '%' . $search .'%');
                });
            }

            $total = $resource->count();
            $resource = $resource->skip($offset)->take($rows);
            $resource = $resource->get()->map(function($val){
                $val->beneficiary_account;
                $val->beneficiary_address;
                $val->beneficiary_training_skill;
                $val->beneficiary_detail;
                return $val;
            });

            return $this->respondSuccessful('Successful', ['rows' => $resource, 'total' => $total]);

        }catch(\Exception $e){
            return $this->respondInternalServerError('There is an error.');
        }
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = new BeneficiaryStoreValidator();

        try {
            DB::beginTransaction();

            $validator->handle($request->toArray());
            $request = $request->toArray();

            $beneficiary = (new \App\Models\Beneficiary())->persist($request);

            $beneficiary->beneficiary_account()->create(
                $this->mapBeneficiaryAccount($request)
            );

            $beneficiary->beneficiary_address()->create(
                $this->mapBeneficiaryAddress($request)
            );

            $beneficiary->beneficiary_detail()->create(
                $this->mapBeneficiaryDetails($request)
            );

            $beneficiary->beneficiary_training_skill()->create(
                $this->mapBeneficiaryTrainingSkill($request)
            );

            DB::commit();
            return $this->respondSuccessful('Beneficiary has been added.', []);

        } catch (InvalidInputException $e) {
            DB::rollback();
            return $this->respondUnprocessable('Failed to add beneficiary.', $validator->getErrors());
        } catch (\Exception $e){
            DB::rollback();
            return $this->respondInternalServerError('There is an error. Please try again later.', $e);
        }
    }

    /**
     * @param array
     * @return array
     */
    private function mapBeneficiaryAccount($request)
    {
        return [
            'account_type' => $request['account_type'],
            'account_number' => $request['account_number']
        ];
    }

    /**
     * @param array
     * @return array
     */
    private function mapBeneficiaryAddress($request)
    {
        return [
            'region_code' => $request['region_code'],
            'province_code' => $request['province_code'],
            'city_code' => $request['city_code'],
            'barangay_code' => $request['barangay_code'],
            'district' => $request['district_number'],
        ];
    }

    /**
     * @param array
     * @return array
     */
    private function mapBeneficiaryDetails($request)
    {
        return [
            'identification_type_id' => $request['identification_type_id'],
            'identification_number' => $request['identification_number'],
            'contact_number' => $request['contact_number'],
            'civil_status' => $request['civil_status'],
            'birth_date' => $request['birth_date'],
            'gender' => $request['gender'],
            'occupation_id' => $request['occupation_id'],
            'occupation_other' => $request['occupation_other'],
        ];
    }

    /**
     * @param array
     * @return array
     */
    private function mapBeneficiaryTrainingSkill($request)
    {
        return [
            'is_interested' => $request['is_interested'] === 'yes',
            'skill' => $request['skill'],
        ];
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $validator = new BeneficiaryUpdateValidator();

        try {
            DB::beginTransaction();

            $validator->handle($request->toArray());
            $request = $request->toArray();

            $beneficiary = (new \App\Models\Beneficiary())->persistUpdate($request, $id);

            $beneficiary->beneficiary_account()->update(
                $this->mapBeneficiaryAccount($request)
            );

            $beneficiary->beneficiary_address()->update(
                $this->mapBeneficiaryAddress($request)
            );

            $beneficiary->beneficiary_detail()->update(
                $this->mapBeneficiaryDetails($request)
            );

            $beneficiary->beneficiary_training_skill()->update(
                $this->mapBeneficiaryTrainingSkill($request)
            );

            DB::commit();
            return $this->respondSuccessful('Beneficiary has been updated.', []);

        } catch (InvalidInputException $e) {
            DB::rollback();
            return $this->respondUnprocessable('Failed to update beneficiary.', $validator->getErrors());
        } catch (\Exception $e){
            DB::rollback();
            return $this->respondInternalServerError('There is an error. Please try again later.', $e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $beneficiary = (new \App\Models\Beneficiary())->find($id);

            $beneficiary->beneficiary_account()->delete();

            $beneficiary->beneficiary_address()->delete();

            $beneficiary->beneficiary_detail()->delete();

            $beneficiary->beneficiary_training_skill()->delete();

            $beneficiary->delete();

            DB::commit();

            return $this->respondSuccessful('Beneficiary has been deleted.', []);

        } catch (\Exception $e){
            DB::rollback();
            return $this->respondInternalServerError('There is an error. Please try again later.', $e);
        }
    }
}
