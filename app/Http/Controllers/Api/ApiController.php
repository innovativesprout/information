<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    protected $status_code = 200;
    protected $message = 'Successful';
    protected $data = [];

    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
        return $this;
    }

    public function setMessage($message)
    {
        if (!is_null($message)){
            $this->message = $message;
        }
        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getStatusCode()
    {
        return $this->status_code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function respond($headers = [])
    {
        return Response::json([
            'status_code' => $this->getStatusCode(),
            'message' => $this->getMessage(),
            'data' => $this->getData()
        ], $this->getStatusCode(), $headers);
    }

    /**
     * @param null $message
     * @param array $data
     * @return mixed
     */
    public function respondSuccessful($message = null, $data = [])
    {
        return $this->setStatusCode(200)->setMessage($message)->setData($data)->respond();
    }

    public function respondSuccessfulNoContent($message = null, $data = [])
    {
        return $this->setStatusCode(204)->setMessage($message)->setData($data)->respond();
    }

    public function respondUnauthorized($message = 'Unauthorized', $data = [])
    {
        return $this->setStatusCode(401)->setMessage($message)->setData($data)->respond();
    }

    public function respondNotFound($message = 'Not Found', $data = [])
    {
        return $this->setStatusCode(404)->setMessage($message)->setData($data)->respond();
    }

    public function respondUnprocessable($message = 'Unprocessable', $data = [])
    {
        return $this->setStatusCode(422)->setMessage($message)->setData($data)->respond();
    }

    public function respondConflict($message = 'Conflict', $data = [])
    {
        return $this->setStatusCode(409)->setMessage($message)->setData($data)->respond();
    }

    public function respondBadRequest($message = 'Bad Request', $data = [])
    {
        return $this->setStatusCode(400)->setMessage($message)->setData($data)->respond();
    }

    public function respondInternalServerError($message = 'Internal Server Error', $data = [])
    {
        return $this->setStatusCode(500)->setMessage($message)->setData($data)->respond();
    }

}
