<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Setup extends Controller
{
    public function init()
    {
        echo "composer: " . shell_exec('cd ../ ; php composer.phar install 2>&1');
    }
}
