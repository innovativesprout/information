<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;

class User extends Controller
{
    public function index()
    {
        return view('pages.users');
    }
}
