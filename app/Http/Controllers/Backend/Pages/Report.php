<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Report extends Controller
{
    public function index()
    {
        return view('pages.report');
    }

    public function store()
    {
        $data = [];
        return (new \App\Services\Report\Report)->handle($data);
    }
}
