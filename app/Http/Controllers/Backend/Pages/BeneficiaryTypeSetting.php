<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BeneficiaryTypeSetting extends Controller
{
    public function index()
    {
        return view('pages.settings.beneficiary-type');
    }
}
