<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Beneficiary extends Controller
{
    public function index()
    {
        return view('pages.beneficiary');
    }

    public function show($id)
    {
        return view('pages.view-beneficiary', ['id' => $id]);
    }
}
