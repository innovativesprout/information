<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportSetting extends Controller
{
    public function index()
    {
        return view('pages.settings.report');
    }
}
