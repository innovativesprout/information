<?php

namespace App\Models;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static updateOrCreate()
 * @method find($id)
 */
class BeneficiaryType extends Model
{
    use HasFactory, Sluggable;

    protected $table = 'beneficiary_types';
    protected $guarded = [];

}
