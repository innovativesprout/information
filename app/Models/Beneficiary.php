<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 */
class Beneficiary extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'beneficiaries';
    protected $guarded = [];
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        $suffix =  $this->suffix_name && !is_null($this->suffix_name) && !empty($this->suffix_name) ? ', ' . $this->suffix_name : '';
        $middle_name =  $this->middle_name && !is_null($this->middle_name) && !empty($this->middle_name) ? ', ' . $this->middle_name : '';
        return $this->attributes['full_name'] = $this->last_name . ', ' . $this->first_name . $middle_name . $suffix;
    }

    public function persist(array $data)
    {
        $resource = $this;
        $resource->first_name = $data['first_name'];
        $resource->middle_name = isset($data['middle_name']) ? $data['middle_name'] : '';
        $resource->last_name = $data['last_name'];
        $resource->suffix_name = $data['suffix_name'];
        $resource->beneficiary_type_id = $data['beneficiary_type_id'];
        $resource->dependent_name = $data['dependent'];
        $resource->save();
        $resource->fresh();
        return $resource;
    }

    public function persistUpdate(array $data, int $id)
    {
        $resource = $this->find($id);
        $resource->first_name = $data['first_name'];
        $resource->middle_name = isset($data['middle_name']) ? $data['middle_name'] : '';
        $resource->last_name = $data['last_name'];
        $resource->suffix_name = $data['suffix_name'];
        $resource->beneficiary_type_id = $data['beneficiary_type_id'];
        $resource->dependent_name = $data['dependent'];
        $resource->save();
        $resource->fresh();
        return $resource;
    }

    public function beneficiary_account()
    {
        return $this->hasOne(BeneficiaryAccount::class);
    }

    public function beneficiary_address()
    {
        return $this->hasOne(BeneficiaryAddress::class);
    }

    public function beneficiary_training_skill()
    {
        return $this->hasOne(BeneficiaryTrainingSkill::class);
    }

    public function beneficiary_detail()
    {
        return $this->hasOne(BeneficiaryDetail::class);
    }
}
