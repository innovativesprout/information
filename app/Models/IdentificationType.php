<?php

namespace App\Models;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 * @method find($id)
 */
class IdentificationType extends Model
{
    use HasFactory, Sluggable, SoftDeletes;

    protected $table = 'identification_types';
    protected $guarded = [];

    public function beneficiary_details()
    {
        return $this->hasMany(BeneficiaryDetail::class, 'identification_type_id', 'id');
    }

}
