<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 */
class BeneficiaryDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'beneficiary_details';
    protected $guarded = [];
    protected $appends = ['age', 'raw_birth_date'];

    public function getBirthDateAttribute($value)
    {
        $date = str_replace('/', '-', $value);
        $date = date('Y-m-d', strtotime($date));
        return date('M d, Y', strtotime($date));
    }

    public function getRawBirthDateAttribute()
    {
        return $this->attributes['raw_birth_date'] = $this->attributes['birth_date'];
    }

    public function getAgeAttribute()
    {
        return $this->attributes['age'] = Carbon::parse($this->attributes['birth_date'])->age;
    }

    public function beneficiary()
    {
        return $this->belongsTo(Beneficiary::class);
    }

    public function identification_type()
    {
        return $this->hasOne(IdentificationType::class, 'id', 'identification_type_id');
    }
}
