<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method ByCityCode($city_code)
 * @method ByProvinceCode($province_code)
 */
class Barangay extends Model
{
    use HasFactory;

    protected $table = 'barangay';
    protected $guarded = [];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_code', 'city_code');
    }

    public function beneficiary()
    {
        return $this->hasMany(Beneficiary::class, 'barangay_code', 'barangay_code');
    }

    public function scopeByCityCode($query, $city_code)
    {
        return $query->where('city_code', $city_code);
    }

    public function scopeByProvinceCode($query, $province_code)
    {
        return $query->where('province_code', $province_code);
    }
}
