<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method ByProvinceCode($province_code)
 * @method get()
 * @method ByRegionCode($region_code)
 */
class Province extends Model
{
    use HasFactory;
    protected $table = 'province';
    protected $guarded = [];

    public function cities()
    {
        return $this->hasMany(City::class, 'province_code', 'province_code');
    }

    public function scopeByProvinceCode($query, $province_code)
    {
        return $query->where('province_code', $province_code);
    }

    public function scopeByRegionCode($query, $region_code)
    {
        return $query->where('region_code', $region_code);
    }
}
