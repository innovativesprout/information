<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static updateOrCreate()
 */
class SettingMeta extends Model
{
    use HasFactory;

    protected $table = 'settings_meta';
    protected $guarded = [];
}
