<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 */
class BeneficiaryTrainingSkill extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'beneficiary_training_skills';
    protected $guarded = [];

    public function beneficiary()
    {
        return $this->belongsTo(Beneficiary::class);
    }
}
