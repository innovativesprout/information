<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method ByCityCode($city_code)
 * @method ByProvinceCode($province_code)
 */
class City extends Model
{
    use HasFactory;

    protected $table = 'cities';
    protected $guarded = [];

    public function barangays()
    {
        return $this->hasMany(Barangay::class, 'city_code', 'city_code');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_code', 'province_code');
    }

    public function scopeByCityCode($query, $city_code)
    {
        return $query->where('city_code', $city_code);
    }

    public function scopeByProvinceCode($query, $province_code)
    {
        return $query->where('province_code', $province_code);
    }
}
