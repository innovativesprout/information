<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 */
class BeneficiaryAddress extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'beneficiary_addresses';
    protected $guarded = [];
    protected $appends = ['complete_address'];

    public function getCompleteAddressAttribute()
    {
        $barangay = $this->barangay->name;
        $city = $this->city->name;
        $province = $this->province->name;
        $region = $this->region->name;
        $complete = strtolower($barangay.', ' .$city.', '.$province);
        return $this->attributes['complete_address'] = ucwords($complete)  . ', '.$region;
    }

    public function beneficiary()
    {
        return $this->belongsTo(Beneficiary::class);
    }

    public function barangay()
    {
        return $this->hasOne(Barangay::class, 'barangay_code', 'barangay_code');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'city_code', 'city_code');
    }

    public function province()
    {
        return $this->hasOne(Province::class, 'province_code', 'province_code');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'region_code', 'region_code');
    }
}
