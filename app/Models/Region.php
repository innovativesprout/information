<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method ByProvinceCode($province_code)
 * @method get()
 */
class Region extends Model
{
    use HasFactory;
    protected $table = 'regions';
    protected $guarded = [];

    public function provinces()
    {
        return $this->hasMany(Province::class, 'region_code', 'region_code');
    }

    public function scopeByRegionCode($query, $region_code)
    {
        return $query->where('region_code', $region_code);
    }
}
