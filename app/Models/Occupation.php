<?php

namespace App\Models;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static updateOrCreate()
 * @method find($id)
 * @property mixed slug
 * @property mixed name
 * @property mixed specified
 */
class Occupation extends Model
{
    use HasFactory, Sluggable, SoftDeletes;

    protected $table = 'occupations';
    protected $guarded = [];

}
