<?php

namespace App\Validators\StoreValidators;

use App\Exceptions\DataExistsException;
use App\Exceptions\InvalidInputException;
use App\Models\IdentificationType;
use App\Models\Occupation;
use App\Validators\ValidatorContract;
use Illuminate\Support\Facades\Validator;

class OccupationStoreValidator implements ValidatorContract
{
    protected $errors = [];

    /**
     * @param array $data
     * @throws InvalidInputException
     * @throws DataExistsException
     */
    public function handle(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required']
        ]);

        if ($validator->fails()){
            $this->errors[] = $validator->errors();
            throw new InvalidInputException;
        }

        if (Occupation::where('name', $data['name'])->first()){
            $this->errors[] = ['name' => ['Name is already exist.']];
            throw new DataExistsException;
        }

    }

    public function getErrors()
    {
        return $this->errors;
    }
}
