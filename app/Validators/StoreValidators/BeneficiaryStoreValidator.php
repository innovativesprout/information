<?php

namespace App\Validators\StoreValidators;

use App\Exceptions\InvalidInputException;
use App\Validators\ValidatorContract;
use Illuminate\Support\Facades\Validator;

class BeneficiaryStoreValidator implements ValidatorContract
{
    protected $errors = [];

    /**
     * @param array $data
     * @throws InvalidInputException
     */
    public function handle(array $data)
    {
        $validator = Validator::make($data, [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'beneficiary_type_id' => ['required'],
            'region_code' => ['required'],
            'province_code' => ['required'],
            'city_code' => ['required'],
            'barangay_code' => ['required'],
            'identification_type_id' => ['required'],
            'identification_number' => ['required'],
            'contact_number' => ['required'],
            'account_type' => ['required'],
            'account_number' => ['required'],
            'civil_status' => ['required'],
            'birth_date' => ['required'],
            'gender' => ['required'],
            'occupation_id' => ['required'],
        ], $this->messages());

        if ($validator->fails()){
            $this->errors[] = $validator->errors();
            throw new InvalidInputException;
        }

        $date = str_replace('/', '-', $data['birth_date']);
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
            $this->errors[] = ['birth_date' => ['Invalid Birth Date']];
            throw new InvalidInputException;
        }

        if ($data['is_interested'] === 'yes'){
            $validator = Validator::make($data, [
                'skill' => ['required']
            ]);

            if ($validator->fails()){
                $this->errors[] = $validator->errors();
                throw new InvalidInputException;
            }
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function messages()
    {
        return [
            'beneficiary_type_id.required' => 'The beneficiary type field is required.',
            'region_code.required' => 'The region field is required.',
            'province_code.required' => 'The province field is required.',
            'city_code.required' => 'The city field is required.',
            'barangay_code.required' => 'The barangay field is required.',
            'identification_type_id.required' => 'The identification type field is required.',
            'occupation_id.required' => 'The occupation field is required.',
        ];
    }
}
