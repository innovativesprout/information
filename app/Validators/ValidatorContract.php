<?php namespace App\Validators;

interface ValidatorContract{
    public function handle(array $data);
    public function getErrors();
}
