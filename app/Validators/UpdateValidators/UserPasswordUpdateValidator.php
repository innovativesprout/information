<?php

namespace App\Validators\UpdateValidators;

use App\Exceptions\DataExistsException;
use App\Exceptions\InvalidInputException;
use App\Models\User;
use App\Validators\ValidatorContract;
use Illuminate\Support\Facades\Validator;

class UserPasswordUpdateValidator implements ValidatorContract
{
    protected $errors = [];

    /**
     * @param array $data
     * @throws InvalidInputException
     */
    public function handle(array $data)
    {
        $validator = Validator::make($data, [
            'password' => ['required', 'min:6', 'max:32'],
            'confirm_password' => ['required', 'same:password'],
        ]);

        if ($validator->fails()){
            $this->errors[] = $validator->errors();
            throw new InvalidInputException;
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
