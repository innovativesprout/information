<?php

namespace App\Validators\UpdateValidators;

use App\Exceptions\DataExistsException;
use App\Exceptions\InvalidInputException;
use App\Models\User;
use App\Validators\ValidatorContract;
use Illuminate\Support\Facades\Validator;

class UserUpdateValidator implements ValidatorContract
{
    protected $errors = [];

    /**
     * @param array $data
     * @throws InvalidInputException
     * @throws DataExistsException
     */
    public function handle(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required'],
            'email' => ['required'],
        ]);

        if ($validator->fails()){
            $this->errors[] = $validator->errors();
            throw new InvalidInputException;
        }

        if (User::where('email', $data['email'])->where('id', '!=', $data['id'])->first()){
            $this->errors[] = ['email' => ['Email is already exist.']];
            throw new DataExistsException;
        }

    }

    public function getErrors()
    {
        return $this->errors;
    }
}
