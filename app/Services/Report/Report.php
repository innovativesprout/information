<?php namespace App\Services\Report;

use Barryvdh\DomPDF\Facade as PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report{

    public function handle($query, $report, $data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle( $report['project_name'] );

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '0000000'],
                ],
            ],
        ];

        $cell_borders_style = ['borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '0000000'],
            ]
        ]
        ];

        $font_size_12 = [
            'font' => [
                'size' => 12,
            ]
        ];

//        $sheet->getColumnDimension("A")->setWidth(20);
//        $sheet->getColumnDimension("B")->setWidth(20);
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->getStyle('B2')->applyFromArray($styleArray);
        $sheet->getStyle('B3')->applyFromArray($styleArray);
        $sheet->getStyle('B4')->applyFromArray($styleArray);
        $sheet->getStyle('B5')->applyFromArray($styleArray);

        $sheet->mergeCells("B1:C1");
        $sheet->mergeCells("B2:C2");
        $sheet->mergeCells("B3:C3");
        $sheet->mergeCells("B4:C4");
        $sheet->mergeCells("B5:C5");

        $sheet->getStyle('A8:N8')->applyFromArray($cell_borders_style);
        $sheet->getStyle('A9:N9')->applyFromArray($cell_borders_style);
        $sheet->getStyle('A8:N8')->applyFromArray($font_size_12);
        $sheet->getStyle('A9:N9')->applyFromArray($font_size_12);

        $sheet->setCellValue('A1', 'Name of Project');
        $sheet->setCellValue('B1', $report['project_name']);
        $sheet->setCellValue('A2', 'DOLE Regional Office');
        $sheet->setCellValue('B2', $report['dole_regional_office']);
        $sheet->setCellValue('A3', 'Province');
        $sheet->setCellValue('B4', $query['province_name']);
        $sheet->setCellValue('A4', 'Municipality');
        $sheet->setCellValue('B4', $query['city_name']);
        $sheet->setCellValue('A5', 'Barangay');
        $sheet->setCellValue('B5', $query['barangay_name']);

        $sheet->mergeCells("A7:N7");
        $sheet->setCellValue("A7", "LIST OF BENEFICIARIES");
        $sheet->getStyle('A7:N7')->getAlignment()->setVertical('center');
        $sheet->getStyle('A7:N7')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A7')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 16
            ]
        ]);

        // Header
        $sheet->mergeCells("A8:A9");
        $sheet->setCellValue("A8", "No.");

        $sheet->mergeCells("B8:E8");
        $sheet->setCellValue("B8", "Name of Beneficiary");

        $sheet->setCellValue("B9", "First Name");
        $sheet->setCellValue("C9", "Middle Name");
        $sheet->setCellValue("D9", "Last Name");
        $sheet->setCellValue("E9", "Extension Name");

        $sheet->mergeCells("F8:F9");
        $sheet->setCellValue("F8", "Birth Date");

        $sheet->mergeCells("G8:J8");
        $sheet->setCellValue("G8", "Address");

        $sheet->setCellValue("G9", "Barangay");
        $sheet->setCellValue("H9", "City/Municipality");
        $sheet->setCellValue("I9", "Province");
        $sheet->setCellValue("J9", "District");

        $sheet->mergeCells("K8:K9");
        $sheet->setCellValue("K8", "Type of I.D.");

        $sheet->mergeCells("L8:L9");
        $sheet->setCellValue("L8", "ID Number");

        $sheet->mergeCells("M8:M9");
        $sheet->setCellValue("M8", "Contact Number");

        $sheet->mergeCells("N8:N9");
        $sheet->setCellValue("N8", "E-payment/Bank Account Number\n(indicate the type of account and number as applicable)");
        $sheet->getStyle('N8')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

        $sheet->getStyle('A8:N8')->getAlignment()->setVertical('center');
        $sheet->getStyle('A8:N8')->getAlignment()->setHorizontal('center');

        $sheet->getStyle('A9:N9')->getAlignment()->setVertical('center');
        $sheet->getStyle('A9:N9')->getAlignment()->setHorizontal('center');

        $starting_row = 10;
        $initial = 0;
        if (count($data) > 0){
            foreach ($data as $index => $item) {
                $initial += 1;

                $sheet->setCellValue("A".$starting_row, $initial);
                $sheet->setCellValue("B".$starting_row, $item['first_name']);
                $sheet->setCellValue("C".$starting_row, $item['middle_name']);
                $sheet->setCellValue("D".$starting_row, $item['last_name']);
                $sheet->setCellValue("E".$starting_row, $item['suffix_name']);
                $sheet->setCellValue("F".$starting_row, $item['beneficiary_detail']['birth_date']);
                $sheet->setCellValue("G".$starting_row, $item['beneficiary_address']['barangay']['name']);
                $sheet->setCellValue("H".$starting_row, $item['beneficiary_address']['city']['name']);
                $sheet->setCellValue("I".$starting_row, $item['beneficiary_address']['province']['name']);
                $sheet->setCellValue("J".$starting_row, $item['beneficiary_address']['district']);
                $sheet->setCellValue("K".$starting_row, $item['beneficiary_detail']['identification_type']['name']);
                $sheet->setCellValue("L".$starting_row, $item['beneficiary_detail']['identification_number']);
                $sheet->setCellValue("M".$starting_row, $item['beneficiary_detail']['contact_number']);
                $sheet->setCellValue("N".$starting_row, $item['beneficiary_account']['account_type'] . ' - ' .$item['beneficiary_account']['account_number']);

                $sheet->getStyle('A'.$starting_row.':N'.$starting_row)->applyFromArray($cell_borders_style);
                $sheet->getStyle('A'.$starting_row.':N'.$starting_row)->applyFromArray($font_size_12);
                $sheet->getStyle('A'.$starting_row.':N'.$starting_row)->getAlignment()->setVertical('center');
                $sheet->getStyle('A'.$starting_row.':N'.$starting_row)->getAlignment()->setHorizontal('center');
                $starting_row += 1;
            }
        }

        $sheet->mergeCells("A".($starting_row + 2).":N".($starting_row + 2));
        $sheet->getStyle("A".($starting_row + 2).":N".($starting_row + 2))->getAlignment()->setWrapText(true);
        $sheet->getStyle("A".($starting_row + 2).":N".($starting_row + 2))->getAlignment()->setIndent(5);
        $sheet->setCellValue("A".($starting_row + 2), $report['footer']);

        $sheet->mergeCells("A".($starting_row + 4).":N".($starting_row + 4));
        $sheet->getStyle("A".($starting_row + 4).":N".($starting_row + 4))->getAlignment()->setWrapText(true);
        $sheet->getStyle("A".($starting_row + 4).":N".($starting_row + 4))->getAlignment()->setIndent(5);
        $sheet->setCellValue("A".($starting_row + 4), $report['sub_footer']);

        // Signature
        $sheet->mergeCells("A".($starting_row + 6).":N".($starting_row + 6));
        $sheet->getStyle("A".($starting_row + 6).":N".($starting_row + 6))->getAlignment()->setWrapText(true);
        $sheet->setCellValue("A".($starting_row + 6), $report['signature_label']);
        $sheet->getStyle("A".($starting_row + 6).":N".($starting_row + 6))->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 12
            ]
        ]);

        $sheet->mergeCells("A".($starting_row + 8).":C".($starting_row + 8));
        $sheet->getStyle("A".($starting_row + 8).":C".($starting_row + 8))->getAlignment()->setWrapText(true);
        $sheet->getStyle("A".($starting_row + 8).":C".($starting_row + 8))->applyFromArray([
            'font' => [
              'size' => 11
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '0000000'],
                ],
            ]
        ]);

        $sheet->mergeCells("A".($starting_row + 9).":B".($starting_row + 9));
        $sheet->getStyle("A".($starting_row + 9).":B".($starting_row + 9))->getAlignment()->setWrapText(true);
        $sheet->getStyle("A".($starting_row + 9).":B".($starting_row + 9))->getAlignment()->setHorizontal('center');
        $sheet->setCellValue("A".($starting_row + 9), $report['signature_title']);
        $sheet->getStyle("A".($starting_row + 9).":B".($starting_row + 9))->applyFromArray([
            'font' => [
              'size' => 10
            ]]);

        $sheet->mergeCells("A".($starting_row + 10).":B".($starting_row + 10));
        $sheet->getStyle("A".($starting_row + 10).":B".($starting_row + 10))->getAlignment()->setWrapText(true);
        $sheet->getStyle("A".($starting_row + 10).":B".($starting_row + 10))->getAlignment()->setHorizontal('center');
        $sheet->setCellValue("A".($starting_row + 10), $report['signature_sub_title']);
        $sheet->getStyle("A".($starting_row + 10).":B".($starting_row + 10))->applyFromArray([
            'font' => [
              'size' => 10
            ]]);


        $writer = new Xlsx($spreadsheet);
        try {
            $path = public_path('files') .'/'. $report['project_name'].date('YmdHis').'.xlsx';
            $writer->save($path);
            return '/files/' . $report['project_name'].date('YmdHis').'.xlsx';
        } catch (Exception $e) {
            return $e;
        }
    }

    public function toXlsx()
    {

    }
}
