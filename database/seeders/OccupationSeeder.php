<?php

namespace Database\Seeders;

use App\Models\Occupation;
use Illuminate\Database\Seeder;

class OccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $data[] = ['name'=>'Transport Workers', 'specified' => false];
        $data[] = ['name'=>'Vendors', 'specified' => false];
        $data[] = ['name'=>'Crop Growers', 'specified' => true];
        $data[] = ['name'=>'Home-based Worker', 'specified' => true];
        $data[] = ['name'=>'Fisher Folks', 'specified' => false];
        $data[] = ['name'=>'Livestock or Poultry Raiser', 'specified' => false];
        $data[] = ['name'=>'Small Transport Driver', 'specified' => false];
        $data[] = ['name'=>'Laborer', 'specified' => true];
        $data[] = ['name'=>'Barangay Tanod', 'specified' => false];
        $data[] = ['name'=>'Barangay Health Worker', 'specified' => false];

        foreach ($data as $d) {
            $resource = new Occupation;
            $resource->slug = $d['name'];
            $resource->name = $d['name'];
            $resource->specified = $d['specified'];
            $resource->save();
        }

    }
}
