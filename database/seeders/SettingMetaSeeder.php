<?php

namespace Database\Seeders;

use App\Models\SettingMeta;
use Illuminate\Database\Seeder;

class SettingMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [];
        $types[] = ['meta_key'=>'project_name', 'meta_value'=>'Tupad'];
        $types[] = ['meta_key'=>'dole_regional_office', 'meta_value'=>''];
        $types[] = ['meta_key'=>'footer', 'meta_value'=>'I hereby certify that the above list of beneficiaries are displaced workers, underemployed or self-employed workers that have lost their livelihood or whose earnings were affected by the COVID-19 pandemic.'];
        $types[] = ['meta_key'=>'sub_footer', 'meta_value'=>"Further, I certify that they or any member of their families were verified to have not received cash assistance from the DOLE’s TUPAD #BKBK and TUPAD as post COVID intervention, COVID Adjustment Measures Program (CAMP),  Abot Kamay Ang Pagtulong (AKAP) for OFWs,   DSWD under the Assistance to Individuals in Crisis Situation (AICS) and the Enhanced Pantawid Pamilyang Pilipino Program (4Ps), DA's cash assistance for rice farmers, and DOF's Small Business Wage Subsidy Program."];
        $types[] = ['meta_key'=>'signature_label', 'meta_value'=>'Prepared and Certified true and Correct by:'];
        $types[] = ['meta_key'=>'signature_title', 'meta_value'=>'LGU  Authorized representative'];
        $types[] = ['meta_key'=>'signature_sub_title', 'meta_value'=>'Signature over Printed Name'];

        foreach ($types as $type) {
            SettingMeta::updateOrCreate(['meta_key' => $type['meta_key']], $type);
        }
    }
}
