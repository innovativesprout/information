<?php

namespace Database\Seeders;

use App\Models\IdentificationType;
use Illuminate\Database\Seeder;

class IdentificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_types = [];
        $id_types[] = ['slug'=>'passport', 'name'=>'Passport', 'description' => null];
        $id_types[] = ['slug'=>'drivers-license', 'name'=>"Driver's License", 'description' => null];
        $id_types[] = ['slug'=>'sss', 'name'=>'SSS', 'description' => null];
        $id_types[] = ['slug'=>'prc-id', 'name'=>'PRC ID', 'description' => null];
        $id_types[] = ['slug'=>'voters-id', 'name'=>"Voter's ID", 'description' => null];
        $id_types[] = ['slug'=>'senior-citizen-id', 'name'=>'Senior Citizen ID', 'description' => null];
        $id_types[] = ['slug'=>'pwd-id', 'name'=>'PWD ID', 'description' => null];
        $id_types[] = ['slug'=>'philhealth-id', 'name'=>'Philhealth ID', 'description' => null];
        $id_types[] = ['slug'=>'tin-card', 'name'=>'TIN Card', 'description' => null];
        $id_types[] = ['slug'=>'postal-id', 'name'=>'Postal ID', 'description' => null];
        $id_types[] = ['slug'=>'gsis-card', 'name'=>'GSIS Card', 'description' => null];

        foreach ($id_types as $id_type) {
            IdentificationType::updateOrCreate(['slug' => $id_type['slug']], $id_type);
        }
    }
}
