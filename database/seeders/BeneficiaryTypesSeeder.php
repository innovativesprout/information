<?php

namespace Database\Seeders;

use App\Models\BeneficiaryType;
use Illuminate\Database\Seeder;

class BeneficiaryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [];
        $types[] = ['slug'=>'self-employed', 'name'=>'Self Employed'];
        $types[] = ['slug'=>'pwd', 'name'=>"PWD"];
        $types[] = ['slug'=>'senior-citizen', 'name'=>"Senior Citizen"];
        $types[] = ['slug'=>'former-rebels', 'name'=>"Former Rebels"];
        $types[] = ['slug'=>'former-violent-extremist-groups', 'name'=>"Former Violent Extremist Groups"];
        $types[] = ['slug'=>'indigenous-people', 'name'=>"Indigenous People"];

        foreach ($types as $type) {
            BeneficiaryType::updateOrCreate(['slug' => $type['slug']], $type);
        }
    }
}
