<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(IdentificationTypeSeeder::class);
        $this->call(BeneficiaryTypesSeeder::class);
        $this->call(OccupationSeeder::class);
        $this->call(CivilStatusSeeder::class);
        $this->call(SettingMetaSeeder::class);
    }
}
