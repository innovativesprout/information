<?php

namespace Database\Seeders;

use App\Models\CivilStatus;
use Illuminate\Database\Seeder;

class CivilStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $data[] = ['name'=>'Single'];
        $data[] = ['name'=>'Married'];
        $data[] = ['name'=>'Separated'];
        $data[] = ['name'=>'Widowed'];

        foreach ($data as $d) {
            $resource = new CivilStatus();
            $resource->slug = $d['name'];
            $resource->name = $d['name'];
            $resource->save();
        }

    }
}
