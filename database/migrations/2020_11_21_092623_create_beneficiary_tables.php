<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiaryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiaries', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('suffix_name')->nullable();
            $table->unsignedBigInteger('beneficiary_type_id');
            $table->string('dependent_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('beneficiary_addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('beneficiary_id');
            $table->string('barangay_code');
            $table->string('city_code');
            $table->string('province_code');
            $table->string('region_code');
            $table->string('district')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('beneficiary_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('beneficiary_id');
            $table->string('birth_date');
            $table->string('contact_number');
            $table->unsignedBigInteger('identification_type_id');
            $table->string('identification_number');
            $table->unsignedBigInteger('occupation_id');
            $table->longText('occupation_other')->nullable();
            $table->string('gender');
            $table->string('civil_status');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('beneficiary_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('beneficiary_id');
            $table->string('account_type');
            $table->string('account_number');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('beneficiary_training_skills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('beneficiary_id');
            $table->boolean('is_interested')->default(false);
            $table->string('skill')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('identification_types', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiaries');
        Schema::dropIfExists('beneficiary_addresses');
        Schema::dropIfExists('beneficiary_details');
        Schema::dropIfExists('beneficiary_accounts');
        Schema::dropIfExists('beneficiary_training_skills');
        Schema::dropIfExists('identification_types');
    }
}
