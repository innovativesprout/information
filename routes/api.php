<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function(){
    Route::post('reports', 'Api\Report@index');
    Route::post('reports/export', 'Api\Report@export');

    Route::post('users/change-password', 'Api\User@changePassword');
    Route::resource('users', 'Api\User', ['only' => ['index', 'store', 'update', 'destroy']]);

    Route::resource('regions', 'Api\Region', ['only' => ['index']]);
    Route::resource('provinces', 'Api\Province', ['only' => ['index']]);
    Route::resource('cities', 'Api\City', ['only' => ['index']]);
    Route::resource('barangays', 'Api\Barangay', ['only' => ['index']]);
    Route::resource('identification-types', 'Api\IdentificationType', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('beneficiary-types', 'Api\BeneficiaryType', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('occupations', 'Api\Occupation', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('beneficiaries', 'Api\Beneficiary', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('civil-statuses', 'Api\User', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('setting-metas', 'Api\SettingMeta', ['only' => ['index', 'store']]);
});
