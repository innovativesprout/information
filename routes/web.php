<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'Backend\Pages\Beneficiary@index');
Route::get('/login', function(){
    return view('layouts.auth');
});
Route::get('/admin/beneficiaries','Backend\Pages\Beneficiary@index');
Route::get('/admin/reports','Backend\Pages\Report@index');

Route::get('/admin/users','Backend\Pages\User@index');

Route::get('/admin/report-settings','Backend\Pages\ReportSetting@index');
Route::get('/admin/beneficiary-types','Backend\Pages\BeneficiaryTypeSetting@index');
Route::get('/admin/identification-types','Backend\Pages\IdentificationTypeSetting@index');
Route::get('/admin/occupations','Backend\Pages\OccupationSetting@index');
Route::get('/admin/civil-status','Backend\Pages\CivilStatusSetting@index');



