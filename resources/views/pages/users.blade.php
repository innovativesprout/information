@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="User">
        <user></user>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/UserRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/user/User.js?v={{$t}}"></script>
@endsection
