@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="Report">
        <report></report>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/RegionRepository.js?v={{$t}}"></script>
    <script src="/app/repository/ProvinceRepository.js?v={{$t}}"></script>
    <script src="/app/repository/CityRepository.js?v={{$t}}"></script>
    <script src="/app/repository/BarangayRepository.js?v={{$t}}"></script>
    <script src="/app/repository/IdentificationTypeRepository.js?v={{$t}}"></script>
    <script src="/app/repository/BeneficiaryTypeRepository.js?v={{$t}}"></script>
    <script src="/app/repository/OccupationRepository.js?v={{$t}}"></script>
    <script src="/app/repository/BeneficiaryRepository.js?v={{$t}}"></script>
    <script src="/app/repository/ReportRepository.js?v={{$t}}"></script>
    <script src="/app/repository/SettingMetaRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/report/Report.js?v={{$t}}"></script>
@endsection
