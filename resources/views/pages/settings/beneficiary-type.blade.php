@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="BeneficiaryTypeSetting">
        <beneficiary-type-setting></beneficiary-type-setting>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/BeneficiaryTypeRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/settings/beneficiary_type_setting/BeneficiaryTypeSetting.js?v={{$t}}"></script>
@endsection
