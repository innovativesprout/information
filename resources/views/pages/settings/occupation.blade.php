@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="OccupationSetting">
        <occupation-setting></occupation-setting>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/OccupationRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/settings/occupation_setting/OccupationSetting.js?v={{$t}}"></script>
@endsection

