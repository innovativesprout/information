@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="ReportSetting">
        <report-setting></report-setting>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}

    {{-- Repository --}}
    <script src="/app/repository/SettingMetaRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/settings/report_setting/ReportSetting.js?v={{$t}}"></script>
@endsection
