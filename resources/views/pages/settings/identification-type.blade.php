@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="IdentificationTypeSetting">
        <identification-type-setting></identification-type-setting>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/IdentificationTypeRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/settings/identification_type_setting/IdentificationTypeSetting.js?v={{$t}}"></script>
@endsection
