@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp
@extends('layouts.backend')

@section('page-styles')

@endsection

@section('content')
    <span ng-app="CivilStatusSetting">
        <civil-status-setting></civil-status-setting>
    </span>
@endsection

@section('page-scripts')

@endsection

@section('ng-scripts')
    {{-- ANGULARJS --}}
    {{--  Plugins  --}}
    <script src="/vendor/framework/angular/angular-mask.min.js"></script>

    {{-- Repository --}}
    <script src="/app/repository/CivilStatusRepository.js?v={{$t}}"></script>

    {{-- Component --}}
    <script src="/app/component/settings/civil_status/CivilStatusSetting.js?v={{$t}}"></script>
@endsection

