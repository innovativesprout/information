@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Innovative Sprout - @yield('page-title')</title>

    <link rel="stylesheet" href="/vendor/theme/assets/css/admin.css">
    <link id="skin-default" rel="stylesheet" href="/vendor/theme/assets/css/theme.css">

    <link rel="stylesheet" href="{{ asset('vendor/app/loader.css') }}?t={{$t}}">

    {{-- textAngular styles --}}
    <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.min.css'>

    @yield('page-styles')

</head>
<body class="nk-body npc-crypto ui-clean pg-auth">

<!-- app body @s -->
<div class="nk-app-root">
    <div class="nk-split nk-split-page nk-split-md">
        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container">
            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                <a href="#" class="toggle btn-white btn btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
            </div>
            <div class="nk-block nk-block-middle nk-auth-body">
                <div class="brand-logo pb-5">
                    <a href="/" class="logo-link">
                        <img class="logo-light logo-img logo-img-lg" src="/vendor/theme/images/logo.png" srcset="/vendor/theme/images/logo2x.png 2x" alt="logo">
                        <img class="logo-dark logo-img logo-img-lg" src="/vendor/theme/images/logo-dark.png" srcset="/vendor/theme/images/logo-dark2x.png 2x" alt="logo-dark">
                    </a>
                </div>
                <div class="nk-block-head">
                    <div class="nk-block-head-content">
                        <h5 class="nk-block-title">Sign-In</h5>
                        <div class="nk-block-des">
                            <p>Access the InnovativeSprout's Information panel using your email and passcode.</p>
                        </div>
                    </div>
                </div><!-- .nk-block-head -->
                <form action="#">
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="default-01">Email or Username</label>
                        </div>
                        <input type="text" class="form-control form-control-lg" id="default-01" placeholder="Enter your email address or username">
                    </div><!-- .foem-group -->
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password">Passcode</label>
                        </div>
                        <div class="form-control-wrap">
                            <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                            </a>
                            <input type="password" class="form-control form-control-lg" id="password" placeholder="Enter your passcode">
                        </div>
                    </div><!-- .foem-group -->
                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block">Sign in</button>
                    </div>
                </form><!-- form -->

            </div><!-- .nk-block -->
            <div class="nk-block nk-auth-footer">
                <div class="nk-block-between">
                    <ul class="nav nav-sm">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Terms & Condition</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Privacy Policy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Help</a>
                        </li>
                    </ul><!-- .nav -->
                </div>
                <div class="mt-3">
                    <p>&copy; {{date('Y')}} InnovativeSprout. All Rights Reserved.</p>
                </div>
            </div><!-- .nk-block -->
        </div><!-- .nk-split-content -->
        <div class="nk-split-content nk-split-stretch bg-lighter d-flex toggle-break-lg toggle-slide toggle-slide-right" data-content="athPromo" data-toggle-screen="lg" data-toggle-overlay="true">
            <div class="slider-wrap w-100 w-max-550px p-3 p-sm-5 m-auto">
                <div class="slider-init" data-slick='{"dots":true, "arrows":false}'>
                    <div class="slider-item">
                        <div class="nk-feature nk-feature-center">
                            <div class="nk-feature-img">
                                <img class="round" src="/vendor/theme/images/slides/promo-a.png" srcset="/vendor/theme/images/slides/promo-a2x.png 2x" alt="">
                            </div>
                            <div class="nk-feature-content py-4 p-sm-5">
                                <h4>Dashlite</h4>
                                <p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
                            </div>
                        </div>
                    </div><!-- .slider-item -->
                    <div class="slider-item">
                        <div class="nk-feature nk-feature-center">
                            <div class="nk-feature-img">
                                <img class="round" src="/vendor/theme/images/slides/promo-b.png" srcset="/vendor/theme/images/slides/promo-b2x.png 2x" alt="">
                            </div>
                            <div class="nk-feature-content py-4 p-sm-5">
                                <h4>Dashlite</h4>
                                <p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
                            </div>
                        </div>
                    </div><!-- .slider-item -->
                    <div class="slider-item">
                        <div class="nk-feature nk-feature-center">
                            <div class="nk-feature-img">
                                <img class="round" src="/vendor/theme/images/slides/promo-c.png" srcset="/vendor/theme/images/slides/promo-c2x.png 2x" alt="">
                            </div>
                            <div class="nk-feature-content py-4 p-sm-5">
                                <h4>Dashlite</h4>
                                <p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
                            </div>
                        </div>
                    </div><!-- .slider-item -->
                </div><!-- .slider-init -->
                <div class="slider-dots"></div>
                <div class="slider-arrows"></div>
            </div><!-- .slider-wrap -->
        </div><!-- .nk-split-content -->
    </div><!-- .nk-split -->
</div><!-- app body @e -->


{{-- DEFAULT LIBRARIES --}}
<script src="/vendor/theme/assets/js/bundle.js"></script>
<script src="/vendor/theme/assets/js/scripts.js"></script>

@yield('page-scripts')

{{-- ANGULARJS --}}
<script src="{{ asset('vendor/framework/angular/angular.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-animate.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-touch.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-typehead.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/ui-bootstrap-custom-2.5.0.js') }}"></script>
<script src="{{ asset('app/AngularVersioning.js') }}"></script>
<script src="{{ asset('app/SwalError.js') }}"></script>

@yield('ng-scripts')

</body>
</html>
