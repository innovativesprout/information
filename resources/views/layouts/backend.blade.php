@php
    $t = \Carbon\Carbon::now()->timestamp;
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Innovative Sprout - @yield('page-title')</title>

    <link rel="stylesheet" href="/vendor/theme/assets/css/admin.css">
    <link id="skin-default" rel="stylesheet" href="/vendor/theme/assets/css/theme.css">

    <link rel="stylesheet" href="{{ asset('vendor/app/loader.css') }}?t={{$t}}">

    {{-- textAngular styles --}}
    <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.min.css'>

    @yield('page-styles')

</head>
<body class="nk-body bg-lighter npc-general has-sidebar ">

<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">

        @include('partials.sidebar')

        <div class="nk-wrap ">

            @include('partials.header')

            <div class="nk-content ">
                <div class="container-fluid">
                    <div class="nk-content-inner">
                        <div class="nk-content-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>

            @include('partials.footer')

        </div>

    </div>

</div>


{{-- DEFAULT LIBRARIES --}}
<script src="/vendor/theme/assets/js/bundle.js"></script>
<script src="/vendor/theme/assets/js/scripts.js"></script>

@yield('page-scripts')

{{-- ANGULARJS --}}
<script src="{{ asset('vendor/framework/angular/angular.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-animate.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-touch.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/angular-typehead.js') }}"></script>
<script src="{{ asset('vendor/framework/angular/ui-bootstrap-custom-2.5.0.js') }}"></script>
<script src="{{ asset('app/AngularVersioning.js') }}"></script>
<script src="{{ asset('app/SwalError.js') }}"></script>

@yield('ng-scripts')

</body>
</html>
