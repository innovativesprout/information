<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="/" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="/images/sendthat_logo.svg" alt="logo" style="height: 120px;">
                <img class="logo-dark logo-img" src="/images/sendthat_logo.svg"  alt="logo-dark" style="height: 120px;">
            </a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">
                    <li class="nk-menu-item">
                        <a href="/admin/beneficiaries" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-file-check"></em></span>
                            <span class="nk-menu-text">Beneficiaries</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/reports" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-report"></em></span>
                            <span class="nk-menu-text">Report</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/users" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                            <span class="nk-menu-text">Users</span>
                            <span class="nk-menu-badge">Soon</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Settings</h6>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="/admin/report-settings" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-report"></em></span>
                            <span class="nk-menu-text">Report Settings</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/beneficiary-types" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-user-alt"></em></span>
                            <span class="nk-menu-text">Beneficiary Types</span>
                            <span class="nk-menu-badge">New</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/identification-types" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-info-fill"></em></span>
                            <span class="nk-menu-text">Identification Types</span>
                            <span class="nk-menu-badge">New</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/occupations" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-unarchive"></em></span>
                            <span class="nk-menu-text">Occupations</span>
                            <span class="nk-menu-badge">New</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                        <a href="/admin/civil-status" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-edit-fill"></em></span>
                            <span class="nk-menu-text">Civil Status</span>
                            <span class="nk-menu-badge">New</span>
                        </a>
                    </li><!-- .nk-menu-item -->

                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->
