<!-- footer @s -->
<div class="nk-footer">
    <div class="container-fluid">
        <div class="nk-footer-wrap">
            <div class="nk-footer-copyright"> &copy; {{ date('Y') }} SendThat.</div>
        </div>
    </div>
</div>
<!-- footer @e -->
