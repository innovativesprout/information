<!-- main header @s -->
<div class="nk-header nk-header-fixed is-light">
    <div class="container-fluid">
        <div class="nk-header-wrap">
            <div class="nk-menu-trigger d-xl-none ml-n1">
                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
            </div>
            <div class="nk-header-brand d-xl-none">
                <a href="#" class="logo-link">
                    <img class="logo-light logo-img" src="/images/sendthat_logo.svg">
                    <img class="logo-dark logo-img" src="/images/sendthat_logo.svg">
                    <span class="nio-version">General</span>
                </a>
            </div><!-- .nk-header-brand -->
            <div class="nk-header-tools">
                <ul class="nk-quick-nav">
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="user-toggle">
                                <div class="user-avatar sm">
                                    <em class="icon ni ni-user-alt"></em>
                                </div>
                                <div class="user-info d-none d-md-block">
                                    <div class="user-status">
{{--                                        @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')--}}
{{--                                            Administrator--}}
{{--                                        @else--}}
{{--                                            Customer--}}
{{--                                        @endif--}}
                                    </div>

                                    <div class="user-name dropdown-indicator">
{{--                                        {{ trim(\Illuminate\Support\Facades\Auth::user()->firstname.' '.\Illuminate\Support\Facades\Auth::user()->lastname) }}--}}
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                <div class="user-card">
                                    <div class="user-avatar">
                                        <span>AB</span>
                                    </div>
                                    <div class="user-info">
                                        <span class="lead-text">
{{--                                            {{ trim(\Illuminate\Support\Facades\Auth::user()->firstname.' '.\Illuminate\Support\Facades\Auth::user()->lastname) }}--}}
                                        </span>
                                        <span class="sub-text">
{{--                                            {{ \Illuminate\Support\Facades\Auth::user()->email }}--}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-inner">
                                <ul class="link-list">
                                    <li><a href="/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li><!-- .dropdown -->
                </ul><!-- .nk-quick-nav -->
            </div><!-- .nk-header-tools -->
        </div><!-- .nk-header-wrap -->
    </div><!-- .container-fliud -->
</div>
<!-- main header @e -->
